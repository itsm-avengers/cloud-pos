import { Component, Input } from "@angular/core";
import { ConnectorService } from "./connector.service";
import { InputComponent } from "./input.component";

@Component({
  selector: 'numpad',
  templateUrl: 'pad.html',
})
export class NumpadComponent {

    @Input() length:number = 0;
    
    constructor(public connector: ConnectorService) {

    }

    fire(num) {
        if(this.length == 0){
            if (this.connector.focused.value !== undefined) {
                var value: string = this.connector.focused.value + '';
                if (value.length < 15) {
                    if (this.connector.focused instanceof InputComponent && value == "0")
                        value = '';
                    this.connector.focused.value = this.connector.focused instanceof InputComponent?parseInt(value + num):(value + num);
                }
            }
        }else{
            if (this.connector.focused.value !== undefined) {
                var value: string = this.connector.focused.value + '';
                if (value.length < 15 && this.length > value.length) {
                    if (this.connector.focused instanceof InputComponent && value == "0")
                        value = '';
                    this.connector.focused.value = this.connector.focused instanceof InputComponent?parseInt(value + num):(value + num);
                }
            }
        }
    }

    fireClear() {
        if (this.connector.focused.value !== undefined) {
            var value: string = (this.connector.focused.value+'').slice(0, -1);;
            if (this.connector.focused instanceof InputComponent && value == '')
                value = '0';
            this.connector.focused.value = this.connector.focused instanceof InputComponent?parseInt(value):(value);
        }
    }

    fireClearAll() {
        if (this.connector.focused.value !== undefined)
            this.connector.focused.value = this.connector.focused instanceof InputComponent?0:'';
    }
}