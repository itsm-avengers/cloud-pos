import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { FCM } from '@ionic-native/fcm';
import { Device } from '@ionic-native/device';

import { ApolloModule, Apollo } from 'apollo-angular';
import { setContext } from 'apollo-link-context';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { MyApp } from './app.component';
import { Log } from '../services/log';
import { Pay } from '../services/pay';
import { Setting } from '../services/setting';
import { HttpService } from '../services/http.service';
import { Utility } from '../services/util';
import { Push } from '../services/push';
import { User } from '../services/user';
import { Sales } from '../services/sales';
import { Membership } from '../services/membership';
import { NetworkStatusService } from '../services/network-status.service';
import { NativeStorage } from '@ionic-native/native-storage';
import { HttpModule } from "@angular/http";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Auth } from '../services/auth';
import { LoginSession } from '../services/login.session';
import { Review } from '../services/review';
import { ImageProvider } from '../providers/image/image-provider';
import { GRAPHQL_ENDPOINT, SUBSCRIPTIONS_ENDPOINT } from '../config';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { orderNum } from '../services/orderNum';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Connect } from '../services/connect';
import { CatProtocolProvider } from '../providers/cat-protocol/cat-protocol';

import { getMainDefinition } from 'apollo-utilities';
import { WebSocketLink } from 'apollo-link-ws';
import { onError } from 'apollo-link-error';

// AoT requires an exported function for factories
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      mode: 'ios',
      preloadModules: true
    }),
    BrowserAnimationsModule,
    HttpModule,
    ApolloModule,
    HttpLinkModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: (createTranslateLoader),
          deps: [HttpClient]
      }
  })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    HttpService,
    Log,
    Pay,
    Setting,
    Sales,
    Utility,
    Push,
    User,
    Membership,
    NetworkStatusService,
    NativeStorage,
    Auth,
    LoginSession,
    NativeStorage,
    ImageProvider,
    Review,
    FCM,
    Device,
    orderNum,
    BarcodeScanner,
    HttpService,
    Connect,
    CatProtocolProvider
  ]
})

export class AppModule {
  constructor(apollo: Apollo, httpLink: HttpLink) {
    var authLink = setContext((_, { headers }) => {
      var token = localStorage.getItem('access_token');

      return {
        headers: {
          ...headers,
          Authorization: token ? `Bearer ${token}` : '',
        }
      }
    });
    const errorLink = onError(({graphQLErrors, networkError})=>{
      if(graphQLErrors){
        console.log("graphql 에러");
        console.log(graphQLErrors);
      }
      if(networkError){
        console.log("네트워크 에러");
        console.log(networkError);
      }
    })

    const uri = GRAPHQL_ENDPOINT
    const http = httpLink.create({uri})
    const ws = new WebSocketLink({
      uri: SUBSCRIPTIONS_ENDPOINT,
      options: {
        lazy:true,
        timeout:60000,
        reconnect: true,
        connectionParams: () => {
          var token = localStorage.getItem('access_token');
          console.log("connectionparam : " +token);
          return (
            {
              Authorization : `Bearer ${token}`
            }
          );
        }
      }
    });

    apollo.create({
      link: authLink.split(
          ({query}) => {
            const {kind, operation} = getMainDefinition(query);
            return kind === 'OperationDefinition' && operation === 'subscription';
          },
          ws,
          http,
        ).concat(errorLink),
      cache: new InMemoryCache(),
      connectToDevTools:true
    });
  }
}
