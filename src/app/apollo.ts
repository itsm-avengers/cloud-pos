// import { ApolloClient, createBatchingNetworkInterface } from 'apollo-client';
// import { SubscriptionClient, addGraphQLSubscriptions } from 'subscriptions-transport-ws/dist/client';
// import 'whatwg-fetch';

// const networkInterface = createBatchingNetworkInterface({
//   // uri: 'http://localhost:3000/graphql',
//   // uri: 'http://192.168.0.15:9001/graphql',
//   // uri: 'http://192.168.0.47:3003/graphql',
//   uri: 'http://homiego.hssa.me:3003/graphql',
//   // uri: 'http://sarang.pohang.go.kr:3000/graphql',
//   batchInterval: 10
// });

// const wsClient = new SubscriptionClient('ws://homiego.hssa.me:3003/subscriptions', {
//   reconnect: true,
// });

// wsClient.onReconnect((off)=>{
//   if (networkStatus)
//     networkStatus.websoketStatus = true;
// });

// wsClient.onDisconnect((off)=>{
//   if (networkStatus)
//     networkStatus.websoketStatus = false;
// });

// let loginSession: any = {
//     getToken: ()=>null
// };

// let networkStatus: any = null;

// networkInterface.use([{
//   applyBatchMiddleware(req, next) {
//     if (!req.options.headers) {
//       req.options.headers = {};  // Create the header object if needed.
//     }
//     // get the authentication token from local storage if it exists
//     if (loginSession.getToken() != null)
//       req.options.headers.Authorization = loginSession.getToken().access_token;
//     next();
//   }
// }]);

// networkInterface.useAfter([{
//   applyBatchAfterware(response: any, next) {
//     if (loginSession && response.status === 401) {
//       loginSession.clean();
//     }
//     next();
//   }
// }]);

// const networkInterfaceWithSubscriptions = addGraphQLSubscriptions(
//   networkInterface,
//   wsClient,
// );

// const client = new ApolloClient({
//   networkInterface: networkInterfaceWithSubscriptions,
//   dataIdFromObject: (o:any) => `${o.__typename}-${o.id},`
// });

// export function provideClient(): ApolloClient {
//   return client;
// }

// export function setLoginSession(session: any) {
//     loginSession = session;
// }

// export function setNetworkStatus(service: any) {
//   networkStatus = service;
// }
