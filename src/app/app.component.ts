import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Events, NavController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginSession } from '../services/login.session';
import { Sales } from '../services/sales';
import { Auth } from '../services/auth';
import { Device } from '@ionic-native/device';
import { FCM } from '@ionic-native/fcm';
import { TranslateService } from '@ngx-translate/core';
import { Connect } from '../services/connect';
// import { CATIP } from '../config';


export enum TYPE {
  ORDER_CHANGED,
  ORDER_DELETED,
  IMMEDIATE_REPAIR_START,
  IMMEDIATE_REPAIR_END,
  UPDATE_REQUIRED,
  MENU_REFRESHED
}

// declare const chrome;
//declare const GigagenieApi;

@Component({
  templateUrl: 'app.html'
})

/**
 *  2018. 06. 29. by 전영찬
 * 1. 선/후불 여부 체크하기
 *    services/login.session의 LoginSession 클래스 import
 *    constructor에 public loginSession과 같이 변수를 만들어 놓기
 *    사용할 때에는 this.loginSession.getInfo().posType의 값을 따져 1이면 선불, 2이면 후불
 *    해당 값이 설정 페이지에서만 변경되므로 매번 불러오지 않고 사용할 수 있게 prepayment라는 변수에 값을 저장해놓은 클래스도 있음(다는 아님. 하지만 똑같이 작동함)
 *    this....posType을 일괄적으로 prepayment로 바꿨다가 오류 난 적이 있어서 수정하려면 하나하나 해야 함
 * 
 * 2. 결제단말기 ip주소 설정
 *    app.component.ts의 생성자에서 설정 가능
 *    단말기의 ip주소 확인은 단말기에서 확인해야 함(현재 기기 비밀번호 7001)
 * 
 * 3. 개발 도중 자동 로그인 설정
 *    login.ts에서 설정 가능
 *    지금은 태블릿에서는 자동 로그인 안 됨
 * 
 * 4. 함수가 너무 많아 생기는 multiDex 오류 수정
 *    프로젝트 내 안드로이드 플랫폼 설치할 때에는 세팅이 안 되어 있어서 플랫폼 깔고 나면 해줘야 함
 *    https://developer.android.com/studio/build/multidex?hl=ko 참조해서 build.gradle 바꾸고
 *    cordova clean android 한 번 실행한 후 빌드하면 문제 해결
 * 
 * 5. cordova-plugin-smtcat
 *    어디서 받아다 설치한 건지 기억이 안 나는데 이걸 프로젝트 폴더 적당한 위치에 넣은 다음
 *    cordova plugin add (경로) 하는 식으로 설치해야 함
 * 
 * 6. 누가 처음에 amount를 가격이라고 해 놔서 AMT 적힌 변수나 속성들은 수량이 아니라 가격임. 수량은 CNT
 * 
 * order.html
 *    클래스 table-body에서 선후불 여부를 따져 출력하는 엘리먼트가 다르게 했음. 형태는 같은데 선불용 정보를 저장하는 위치가 달라서 두 개를 따로 만듦
 * 
 * order.ts
 *    graphql 바뀐 곳이 많아서 이전 브랜치 것은 버려야 함
 *    결제와 관련된 전반적인 부분은 선후불 여부를 따져야 해서 다른 브랜치에서 하나로 퉁치던 것을 if-else문으로 나눠놓은 곳이 많음
 *    prepaymentFloor
 *        무엇 때문이었는지 만들어놨는데 지금 사용하는 곳 없는 변수라서 코드 보고 그냥 주석처리 해버려도 됨
 *    tableTags
 *        선불로 설정되는 경우 화면 오른쪽에 보이는 선불 테이블용 변수
 *        주문한 메뉴의 정보는 ['detail']에 들어감
 * 
 * pay.ts
 *    order.ts에서 결제정보 가져올 때엔 this.navParams.get('details') 호출하면 됨
 *        order.ts의 goToPay()에서 넘기는 데이터임 
 *        
 * 
 * <<현재 이슈>>
 *    카드 결제 요청을 했을 때
 *        1) 결제에 실패했다며 modal 뜬 게 바로 내려가거나 
 *        2) 단말기에서 아무런 반응도 안 하고, '카드를 넣어주세요'라는 메시지에서 확인 누르면 정상 결제로 처리되면서 앱 종료하면 그제서야 단말기에서 삐빅 소리가 남
 */

export class MyApp {

  @ViewChild('nav') nav:NavController
  
  rootPage: string = 'ContainerPage';
  selectedIdx: number = -1;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private menuCtrl: MenuController, public loginSession: LoginSession, private events: Events, public sales: Sales, private auth: Auth, fcm: FCM, private device: Device, private trans: TranslateService, public connect: Connect) {

      this.trans.setDefaultLang('cn');
      this.trans.addLangs(['ko', 'en', 'cn']);
      this.trans.use('en')
      .subscribe(_=>this.trans.use('ko'));

    platform.ready().then(() => {
      //결제 단말기 test 용입니다. 
      if (platform.is('mobile')) {
        // chrome.socket.create('tcp', (createInfo) => {
        //  let socketId = createInfo.socketId;
        //   console.log(`Sockid: ${socketId}`);
        //   chrome.socket.connect(socketId, CATIP, 5555, (result) => {
        //     console.assert(0 === result); // listen success
        //     console.log(`Listen: ${result}`);
        //     // this.events.publish('pay:accept');
        //     console.log(socketId);
        //     localStorage.setItem('socketId', socketId);
        //     if(result == 0){
        //       this.connect.connect=true;
        //     }
        //   });
        // });

      }
      //여기 까지 


      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      localStorage.setItem('lang','ko');
      let topic = localStorage.getItem('topic');
      if (!localStorage.getItem('access_token') || +localStorage.getItem('expires_in') < new Date().getTime()) {
        if (topic)
          fcm.unsubscribeFromTopic(topic);
      }

      fcm.onNotification().subscribe(
        data => {
          if (data.deviceId == this.device.uuid)
            return;

          data.type = `${data.type}`;
          console.log(data);
          switch (data.type) {
            case "ORDER_CHANGED":
              this.events.publish('order:changed', data);
              break;
            case "ORDER_DELETED":
              this.events.publish('order:deleted', data);
              break;
          }
        },
        error => {
          console.log(error);
        }
      );

      this.menuCtrl.swipeEnable(false);

      events.subscribe('nav:index', (idx) => {
        this.selectedIdx = idx;
      });
      events.subscribe('fuction:loginSession', () => {
        this.getLoginSession();
      });
    });

    this.getOpenTm(0);
  }

  go(index) {
    if (index == 0) {
      this.selectedIdx = index;
      this.getOpenTm(this.selectedIdx);
    } else if (index == 6) {
      this.selectedIdx = index;
      this.getOpenTm(this.selectedIdx);
    } else {
      this.events.publish('nav:go', index);
      if (index != 4) {
        this.selectedIdx = index;
      }
      else {
      }
      this.menuCtrl.close();
    }
  }

  getOpenTm(index) {
    //가장 최근의 개시 일자 가지고 오기 
    this.sales.end.getNo()
      .subscribe(
        data => {
          if (index == 0)
            this.events.publish('nav:go', 0);
          else
            this.events.publish('nav:go', 6);
        },
        err => {
          if (err == "network")
            console.log('인터넷 연결을 확인해 주세요.');
          else
            this.events.publish('nav:go', 4);
        }
      );
    this.menuCtrl.close();
  }

  getLoginSession() {
    this.auth.getInfo()
      .map(data => data.json())
      .subscribe(
        data => {
          this.loginSession.setInfo(data);
        }
      );
  }
}

