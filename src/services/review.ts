import { Injectable } from "@angular/core";
import { HttpService } from "./http.service";

@Injectable()
export class Review {
    public reviewDetail: ReviewDetail;
    public reviewComment: ReviewComment;
    public writeComment: WriteComment;

    constructor(private httpService: HttpService) {
        this.reviewDetail = new ReviewDetail(this.httpService);
        this.reviewComment = new ReviewComment(this.httpService);
        this.writeComment = new WriteComment(this.httpService);
    }
}

export class ReviewDetail {
    private resource: string = '/review';

    constructor(private httpService: HttpService) {
    }

    get(pageNo, pageSize, searchTxt) {
        var params = {
            'pageNo': pageNo,
            'pageSize': pageSize,
            'searchTxt': searchTxt
        }
        
        return this.httpService.get(this.resource, params)
    }
}

export class ReviewComment {
    private resource: string = '/review/comment';

    constructor(private httpService: HttpService) {
    }

    get(reviewNo, pageNo=1, pageSize=5) {
        var params = {
            'pageNo': pageNo,
            'pageSize': pageSize,
            'reviewNo': reviewNo
        }

        return this.httpService.get(this.resource, params)
    }
}

export class WriteComment {
    private resource: string = '/review/comment'

    constructor(private httpService: HttpService) {
    }

    post(reviewNo, commentText) {
        var params = {
            'reviewNo': reviewNo,
            'commentText': commentText
        }

        return this.httpService.post(this.resource, params)
    }
}