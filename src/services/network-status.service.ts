import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Observable';
// import { setNetworkStatus } from "../app/apollo";
import gql from 'graphql-tag';
import { Apollo } from 'apollo-angular';
import * as _ from 'lodash';

const Sync = gql`
    mutation Sync($queue: [OrderQuereItem]!) {
        sync(queue: $queue)
    }
`;

const Floor = gql`
  query { 
    getStore{
      id
      floor {
        id
        table {
          id
          order {
            id
            total
            detail {
              id
              menuNo
              name
              quantity
              cost
              optAmt
            }
          }
        }
      }
    }
  }
`;

declare const navigator;

export interface Mutation {
    time: number
    order: OrderInput
    detail: [OrderDetailInput]
}

export interface OrderInput {
    orderNo: number
    orderName: string
    orderType: string
    waitingNo: number
    tableNo: number
}

export interface OrderDetailInput {
    menuNo: number
    menuName: string
    itemAmt: number
    itemCNT: number
}

@Injectable()
export class NetworkStatusService {
    bOnline: boolean = true;
    oId:number = -1;

    constructor(private apollo: Apollo) {
        // setNetworkStatus(this);
        this.networkStatus$.subscribe(
            res=>{
                if (this.bOnline != res && res) {
                    this.syncWithServer();
                }
                this.bOnline = res;
            },
            err => {
                console.log('network status error');
            }   
        );
    }

    networkStatus$ = Observable.timer(0, 1000)
    .timeInterval()
    .map((x) => { 
        return  navigator.onLine && this.websoketStatus;
    });
    websoketStatus: boolean = true;

    private failedMutation: Mutation[] = [];

    addQueue(mutation: Mutation) {
        console.log(503);
        
        this.failedMutation.push(mutation);
        var client = this.apollo.getClient();
        const data: any = _.cloneDeep(client.readQuery({ query: Floor }));
        var newOrder:any;
        if (mutation.order.orderNo) {
            newOrder = null;
            data.getStore.floor.some((floor)=>{
                return floor.table.some(table=>{
                    if (table.order.id == mutation.order.orderNo) {
                        newOrder = table.order;
                        return true;
                    } else {
                        return false;
                    }
                })
            });
            
            this.pushOrder(newOrder, mutation.detail);
        } else {
            var newTable = null;
            data.getStore.floor.some((floor)=>{
                return floor.table.some(table=>{
                    if (table.id == mutation.order.tableNo) {
                        newTable = table;
                        return true;
                    } else {
                        return false;
                    }
                })
            });
            newOrder = {
                __typename: "OrderType",
                id: this.oId--,
                total: 0,
                detail: []
            };
            this.pushOrder(newOrder, mutation.detail);
            newTable.order = newOrder;
        }
        client.writeQuery({
          query: Floor,
          data: data
        });
    }

    pushOrder(newOrder, detail) {
        var total = 0;
        newOrder.detail = detail.map(detail=> {
            total += (detail.itemAmt + detail.optAmt) * detail.itemCNT;
            return {
                __typename: "OrderDetail",
                id: this._objectId(),
                menuNo: detail.menuNo,
                name: detail.menuName,
                quantity: detail.itemCNT,
                cost: detail.itemAmt,
            }
        });
        newOrder.total = total;
    }

    syncWithServer() {
        this.apollo.mutate({
            mutation: Sync,
            variables: {
                queue: this.failedMutation
            }
        }).subscribe(res=>{
            this.cleanQueue();
        });
    }

    cleanQueue() {
        this.failedMutation = [];
    }

    private _objectId() {
        var timestamp = (new Date().getTime() / 1000 | 0).toString(16);
        return timestamp + 'xxxxxxxxxxxxxxxx'.replace(/[x]/g, function() {
            return (Math.random() * 16 | 0).toString(16);
        }).toLowerCase();
    };
}