import { Injectable } from '@angular/core';

@Injectable()
export class Utility {
    constructor() { }
    
    comma(txt) {
        return String(txt).replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
    }
}