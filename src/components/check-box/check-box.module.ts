import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { CheckBoxComponent } from './check-box';
// import { NG_VALUE_ACCESSOR } from "@angular/forms";

@NgModule({
  declarations: [
    CheckBoxComponent
  ],
  imports: [
    IonicModule
  ],
  providers: [],
  exports: [
    CheckBoxComponent
  ]
})
export class CheckboxModule {}