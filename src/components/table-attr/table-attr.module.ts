import { NgModule, forwardRef } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { TableAttrComponent } from './table-attr';
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import { CheckboxModule } from '../../components/check-box/check-box.module';


@NgModule({
  declarations: [
    TableAttrComponent
  ],
  imports: [
    IonicModule,
    CheckboxModule,
  ],
  providers: [],
  exports: [
    TableAttrComponent
  ]
})
export class TableAttrComponentModule {}