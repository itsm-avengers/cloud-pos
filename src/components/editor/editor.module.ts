import { NgModule, forwardRef } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { EditorComponent } from './editor';
import { NG_VALUE_ACCESSOR } from "@angular/forms";


@NgModule({
  declarations: [
    EditorComponent
  ],
  imports: [
    IonicModule,
  ],
  providers: [],
  exports: [
    EditorComponent
  ]
})
export class EditorComponentModule {}