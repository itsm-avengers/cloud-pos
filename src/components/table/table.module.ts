import { NgModule, forwardRef } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { TableComponent } from './table';
import { NG_VALUE_ACCESSOR } from "@angular/forms";
import { CheckboxModule } from '../../components/check-box/check-box.module';


@NgModule({
  declarations: [
    TableComponent
  ],
  imports: [
    IonicModule,
    CheckboxModule,
  ],
  providers: [],
  exports: [
    TableComponent
  ]
})
export class TableComponentModule {}