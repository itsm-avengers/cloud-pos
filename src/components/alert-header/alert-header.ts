import { Component, Input, Output, EventEmitter } from '@angular/core';
// import { ViewController } from 'ionic-angular';

@Component({
  selector: 'alert-header',
  templateUrl: 'alert-header.html'
})
export class AlertHeaderComponent {
  @Input() title = "";
  @Output() closeAction :EventEmitter<any> = new EventEmitter();

  constructor() {
  }

  close() {
    this.closeAction.emit();
  }
}
