import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StampSetPage } from './stamp-set';
import { AlertHeaderComponentModule } from '../../components/alert-header/alert-header.module';
import { CheckboxModule } from '../../components/check-box/check-box.module';
import { NumpadModule } from '../../modules/Numpad/numpad.module';

@NgModule({
  declarations: [
    StampSetPage,
  ],
  imports: [
    IonicPageModule.forChild(StampSetPage),
    AlertHeaderComponentModule,
    CheckboxModule,
    NumpadModule
  ],
  exports: [
    StampSetPage
  ]
})
export class StampSetPageModule {}
