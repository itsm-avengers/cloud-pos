import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MileageLogPage } from './mileage-log';
import { AlertHeaderComponentModule } from "../../components/alert-header/alert-header.module";

@NgModule({
  declarations: [
    MileageLogPage,
  ],
  imports: [
    IonicPageModule.forChild(MileageLogPage),
    AlertHeaderComponentModule
  ],
  exports: [
    MileageLogPage
  ]
})
export class MileageLogPageModule {}
