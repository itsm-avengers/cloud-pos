import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SaveStampContainerPage } from './save-stamp-container';
import { AlertHeaderComponentModule } from "../../components/alert-header/alert-header.module";

@NgModule({
  declarations: [
    SaveStampContainerPage,
  ],
  imports: [
    IonicPageModule.forChild(SaveStampContainerPage),
    AlertHeaderComponentModule
  ],
  exports: [
    SaveStampContainerPage
  ]
})
export class SaveStampContainerPageModule {}
