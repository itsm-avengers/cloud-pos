import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-save-stamp-container',
  templateUrl: 'save-stamp-container.html',
})
export class SaveStampContainerPage {
  pageTitle: string = "스탬프 적립 완료";
  beforeStamp: number = 0;
  afterStamp: number = 0;
  saveOrUse: boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    this.saveOrUse = this.navParams.get('saveOrUse');
    this.beforeStamp = this.navParams.get('beforeStamp');
    this.afterStamp = this.navParams.get('afterStamp');
    if (!this.saveOrUse)
      this.pageTitle = "스탬프 사용 완료";
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

}
