import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Toast, ToastController } from 'ionic-angular';
import { User } from "../../services/user";
import { LoginSession } from '../../services/login.session';
// import { forEach } from 'async';

@IonicPage()
@Component({
  selector: 'page-check-member',
  templateUrl: 'check-member.html',
})
export class CheckMemberPage {
  pageTitle: string = "회원 조회";

  mileageOrStamp: any = 1;

  userList: Array<any> = [];
  tel: any = '';
  selectedUser: any = null;
  result: Array<any> = [];
  private toastInstance: Toast;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, private user: User, private loginSession: LoginSession,public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CheckMemberPage');

    this.tel = this.navParams.get('tel');
    this.mileageOrStamp = this.loginSession.getInfo().mileageStampOpt;

    this.getUser();
  }

  getUser() {
    this.user.get(this.tel).subscribe(
      data => {
        this.userList = data.json();
        console.log(this.userList);
        this.userList.forEach(element =>{
          if(this.tel===element.tel)
          {
            this.result.push(element);
          }
          if(this.result.length==0)
          {
            this.toast('전체 전화번호를 입력해주세요');
          }
          
        });
      }
    )
  }

  selectUser(item) {
    this.selectedUser = item;
    console.log(this.selectedUser);

    let data = this.selectedUser;
    this.viewCtrl.dismiss(data);
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  toast(text) {
    if (this.toastInstance) {
      return;
    }
    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
    });
    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });
    this.toastInstance.present();
  }

}
