import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewTablePage } from './new-table';
import { AlertHeaderComponentModule } from '../../components/alert-header/alert-header.module';
import { CheckboxModule } from '../../components/check-box/check-box.module';
import { NumpadModule } from '../../modules/Numpad/numpad.module';

@NgModule({
  declarations: [
    NewTablePage,
  ],
  imports: [
    IonicPageModule.forChild(NewTablePage),
    AlertHeaderComponentModule,
    CheckboxModule,
    NumpadModule
  ],
  exports: [
    NewTablePage
  ]
})
export class NewTablePageModule {}
