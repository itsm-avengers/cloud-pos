import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmAlertPage } from './confirm-alert';
import { AlertHeaderComponentModule } from '../../components/alert-header/alert-header.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ConfirmAlertPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmAlertPage),
    AlertHeaderComponentModule,
    TranslateModule.forChild()
  ],
  exports: [
    ConfirmAlertPage
  ]
})
export class ConfirmAlertPageModule {}
