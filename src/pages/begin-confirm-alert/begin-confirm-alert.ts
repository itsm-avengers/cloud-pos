import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-begin-confirm-alert',
  templateUrl: 'begin-confirm-alert.html',
})
export class BeginConfirmAlertPage {
  title: string = "";
  salesDt: string = "";
  openCash: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    this.title = this.navParams.get('title');
    this.salesDt= this.navParams.get('salesDt');
    this.openCash = this.navParams.get('openCash');
  }

  close(status) {
    if(status == 1) {
      this.viewCtrl.dismiss({res: true});
    }
    else {
      this.viewCtrl.dismiss({res: false});
    }
  }
}
