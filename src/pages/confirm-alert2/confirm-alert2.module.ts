import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmAlert2Page } from './confirm-alert2';
import { AlertHeaderComponentModule } from '../../components/alert-header/alert-header.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ConfirmAlert2Page,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmAlert2Page),
    AlertHeaderComponentModule,
    TranslateModule.forChild()
  ],
  exports: [
    ConfirmAlert2Page
  ]
})
export class ConfirmAlert2PageModule {}
