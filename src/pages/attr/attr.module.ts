import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AttrPage } from './attr';

@NgModule({
  declarations: [
    AttrPage,
  ],
  imports: [
    IonicPageModule.forChild(AttrPage),
  ],
  exports: [
    AttrPage
  ]
})
export class AttrPageModule {}
