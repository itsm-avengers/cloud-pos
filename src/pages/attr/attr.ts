import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, Toast, IonicPage } from 'ionic-angular';
import { Setting } from '../../services/setting';

@IonicPage()
@Component({
  selector: 'page-attr',
  templateUrl: 'attr.html'
})
export class AttrPage {
  attrList: any = [];
  private toastInstance: Toast;

  constructor(public navCtrl: NavController, public navParams: NavParams, public setting: Setting, public toastCtrl: ToastController) { }

  ionViewDidLoad() {
    this.getList();
    console.log('ionViewDidLoad AttrPage');
  }
  getList() {
    this.setting.attr.get()
      .subscribe(
      data => {
        this.attrList = data.json();
        this.attrList.forEach((element, idx) => {
          element.no = idx;
          element.useYN = element.useYN == 1 ? true : false;
        });
        this.toast('속성정보를 가져왔습니다.');
        console.log(data.json());
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else if (err.json().state == "fail")
          this.toast('오류입니다. 다시시도해 주세요.');
        console.log(err);
      }
      );
  }

  toggle(detailCode) {
    // console.log(detailCode);
    this.setting.attr.post(detailCode)
      .subscribe(
      data => {
        console.log(data.json());
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else
          this.toast('오류입니다. 다시시도해 주세요.');
        console.log(err);
      });
  }

  toast(text) {

    if (this.toastInstance) {
      return;
    }

    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
    });

    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });

    this.toastInstance.present();
  }

}
