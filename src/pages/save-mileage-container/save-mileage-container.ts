import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { PACKAGE_NAME } from '../../config';

@IonicPage()
@Component({
  selector: 'page-save-mileage-container',
  templateUrl: 'save-mileage-container.html',
})
export class SaveMileageContainerPage {
  pageTitle:string ;
  beforemileage:any = 0;
  aftermileage:any = 0;


  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private events: Events, private tran:TranslateService) {
  }

  ionViewDidLoad() {
    this.tran.get('m_title').subscribe((res:string)=>{
      this.pageTitle=res;
    })
    this.beforemileage = this.navParams.get('beforemileage');
    this.aftermileage = this.navParams.get('aftermileage');

    // this.tran.get('accum_tts').subscribe((res:string)=>{
    //   GigagenieApi.ttsSender(res,this.getLangCode(this.tran.currentLang),PACKAGE_NAME)
    // })
 
    console.log('ionViewDidLoad SaveMileageContainerPage');
  }

  getLangCode(lang) {
    switch (lang) {
      case "ko":
      return 0;
      case "en":
      return 1;
      case "cn":
      return 2;
    }
  }

  closeModal() {
    this.events.publish('pay:useSaving', false);
    this.viewCtrl.dismiss();
  }

}
