import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, NavParams, ToastController, ModalController, Events, Toast, Platform, App, IonicPage } from 'ionic-angular';
import { Pay } from '../../services/pay';
import { Sales } from '../../services/sales';
import { User } from '../../services/user';
import { ConnectorService } from "../../modules/Numpad";
import { Observable } from 'rxjs/Observable';
import { LoginSession } from "../../services/login.session";
import { TranslateService } from '@ngx-translate/core';
import { PACKAGE_NAME, IS_GENIE } from '../../config';
import { orderNum } from '../../services/orderNum';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { CatProtocolProvider } from '../../providers/cat-protocol/cat-protocol';
import * as moment from 'moment';

//declare const GigagenieApi;
// declare const BIXOLON;

@IonicPage()
@Component({
  selector: 'page-pay',
  templateUrl: 'pay.html'
})
export class PayPage {
  //genie = IS_GENIE;
  tel: any;
  //주문 정보 
  //실제 계산 해야하는 총액들
  orderNo: number;
  orderAmt: number = 0;// 할인 전금액
  payTotal: number = 0;//할인 후 금액 실제 계산해야하는 금액 
  remainingAmt: number = 0; //촤즉 상단에 전체 금액 (payTotal - 사용한 금액)
  // payTax: any = 0;
  remain: number = 0;  // 재결제 할때 결제 해야하는돈 
  payBlur: boolean = false; // 결제 해도 되는 상태인지 (우측에 블러 반영)

  onego: boolean = true; // 한번에 결제!
  divide: boolean = false; // 나눠서 결제!

  payType: any = { 'card': true, 'cash': false };//어느걸로 결제할지... 
  divideBy: boolean = true; //메뉴별 결제 -true, 인원별 결제 false;
  cashOrcard: boolean =true;
  saving: boolean = true;
  mileageOrStamp: boolean =true;

  //부분 결제시 사용자에게 보여지는 금액  금액 
  divideOrderAmt: number = 0; //부분 결제시 할인 전 금액
  dividePayTotal: number = 0;//부분 결제시 실제 계산 금액 
  cardInstallmentMm: number = 0; //카드 할부 기간 선택
  cashReceived: number = 0; //현금시 잔돈 표시 
  byPerson = [{ orderAmt: 0, payTotal: 0, payYN: false }];
  byMenu = { orderAmt: 0, payTotal: 0, payYN: false };
  byPersonIdx: any = null;
  cashPrintNo: number = null; //현금 영수증 승인 번호 

  //메뉴별 결제할때 표시될 현재 주문의 메뉴 정보들 
  checkAll: boolean = false;
  menuList = [{
    checked: false, //선택 됨을 표시 
    dcAmt: 0, //할인 금액
    dcName: '', //할인 명
    itemAmt: 0, //메뉴의 금액
    itemCNT: 0, //메뉴 개수 
    menuName: '', //메뉴 명
    menuNo: 0, //메뉴 번호 
    orderMemo: '', //주문 메모 // 미구현 
    orderNo: 0, //주문 번호
    page: 0, //pagenation... ? 
    payYN: false
  }];

  saveOrNot: boolean = false;

  //결제 정보
  // payItem: any = [];

  //결제 세금 비율
  // taxRatio: number = 1.1;
  truncValue: number = 10;

  //사용자 정보
  userPhone: string = '';
  userNo: number;
  userCurPoint: number = 0;
  usePoint: number = 0;
  useMileage: number = 0;
  useStamp: number = 0;


  //카드 결제 정보 단말 연결되면 지우기
  cardPayInfo = {
    'payNo': 0,
    'cardID': "0000-0000-0000-000",
    'cardType': "신용",
    'cardName': "삼성골드",
    'cardCompany': "삼성카드",
    'installmentMm': "0",
    'cardYYMM': "00-00",
    'approvalAmt': 0,
    'approvalNo': Math.ceil(Math.random() * 1000),
    'approvalDate': '',
    'terminalID': "1234",
    'terminalStoreID': "181",
    'tranID': 123456,
    terminalYn: false,
    isCompleted: false,
    orderNo: null
  }

  private toastInstance: Toast;
  backBtnPressed: boolean = false;

  //전영찬
  prepayment = this.loginSession.getInfo().posType == 1
  discounted: number = 0
  orderInfo: any = null
  payCashClicked = null//현금결제가 두 번 일어나는 현상 막으려고 만듦

  //전영찬 180620 카드결제 쪽 바뀌면서 그쪽 함수에 패러미터로 넣을 새로운 놈 추가
  orderList = []
  sumPrice = 0

  constructor(public platform: Platform,
              public app: App,
              public navCtrl: NavController,
              public navParams: NavParams,
              public pay: Pay,
              public sales: Sales,
              public user: User,
              public numpad: ConnectorService,
              public toastCtrl: ToastController,
              public modalCtrl: ModalController,
              private events: Events,
              public loginSession: LoginSession,
              private ref: ChangeDetectorRef) {
    // console.log(this.navParams.get('details'))

    //180621 전영찬: confirmalert2쪽으로 데이터 넘ㄴ길 녀석들 폼 맞춤
    var tempDetails = this.navParams.get('details')
    for(let i = 0; i < tempDetails.length; i++){
      this.orderList.push({
        name: tempDetails[i].menuName,
        amt : tempDetails[i].itemAmt - tempDetails[i].dcAmt,
        cnt : tempDetails[i].itemCNT
      })
    }
    for(let i = 0; i< this.orderList.length; i++){
      this.sumPrice += this.orderList[i].amt
    }


    // console.log(this.orderList)

    var timer = null;
    platform.registerBackButtonAction(() => {
      if (!this.backBtnPressed) {
        this.backBtnPressed = true;
        if (timer)
          timer.unsubscribe();
        timer = Observable.timer(250).subscribe(() => {
          this.backBtnPressed = false;
        });

        let view = this.navCtrl.getActive();
        console.log(view.instance);

        if (view.instance instanceof PayPage) {
          if (!this.byPerson.every(isNoPartiallypaid) || !this.menuList.every(isNoPartiallypaid)) {
            let modal = this.modalCtrl.create('ConfirmAlertPage', { title: '결제 취소', label_big: '정말로 진행 중 인 결제를 취소 하시겠습니까?', label_small: '이 후 남은 결제는 조회 화면에서 결제 취소 후 재결제가 가능합니다.', type: 0 }, { cssClass: "message-modal" });
            modal.onDidDismiss((data) => {
              console.log(data);
              if (data.res == true) {
                if (this.navCtrl.length() > 1)
                  this.navCtrl.pop();
              }
            });
            modal.present();
            return 0;
          }
        }

        if (this.navCtrl.length() > 1)
          this.navCtrl.pop();
      }
    }, 500);
    function isNoPartiallypaid(element) {
      return element.payYN == false;
    }
  }

  ionViewDidLoad() {
    this.orderNo = this.navParams.get("orderNo");
    if (this.navParams.get("remain"))
      this.remain = this.navParams.get("remain");
    this.events.publish('layout:header', '결제', false, true, false);

    //여기 내가 추가함
    this.orderInfo = this.navParams.get('orderInfo')

    this.getOrder();
    this.getOrderDetail();

    console.log('ionViewDidLoad PayPage');
  }

  ionViewDidLeave() {
    console.log('ionViewDidLeave PayPage');
    // this.events.unsubscribe('pay:accept');//이것때문에 다음번에 결제 안되는거같기도 ... 
    this.events.publish('layout:backdrop', false);
    this.events.publish('menu-list:refresh');
  }

  showBackdrop(val: boolean) {
    this.events.publish('layout:backdrop', val);
  }

  //page들어오자마자 주문 정보 가지고 오기 
  getOrder() {
    //주문 정보 가지고 오기 
    // console.log(this.orderNo)
    if (this.prepayment) {
      this.prepaymentPrice(true)
    }
    else {
      this.sales.order.get(this.orderNo)
        .subscribe(
        data => {
          if (data.json().length == 0) {
            this.toast('주문 내역을 가지고 올 수 없습니다.');
            this.clearAllNavCtrl();
          }
          else {
            if (this.remain > 0)
              this.cashReceived = this.dividePayTotal = this.remainingAmt = this.payTotal = this.remain;
            else
              this.cashReceived = this.dividePayTotal = this.remainingAmt = this.payTotal = data.json()[0].payTotal;

            this.orderAmt = this.divideOrderAmt = data.json()[0].orderAmt;
            // this.payTax = data.json().paytax; //생각해보기
            console.log(data.json()[0]);
            this.byPerson = [{ orderAmt: this.orderAmt, payTotal: this.payTotal, payYN: false }];
          }
        },
        err => {
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('오류입니다. 다시시도해 주세요.');
        }
        );
    }
  }

  getOrderDetail() {
    //주문에 대한 메뉴 주문 내역 가지고 오기 
    if (this.prepayment) {
      this.menuList = this.navParams.get('orderInfo');
      var detailMenu = [];
      var i, j, k;

      var temp = []

      //메뉴들 개수 별로 다 나눠버리기 
      for (i = 0, j = 0; this.menuList.length > i; i++ , j++) {
        if (this.menuList[i].itemCNT > 1) {
          for (k = 0; this.menuList[i].itemCNT > k; k++) {
            detailMenu[j] = this.menuList[i];
            if (this.menuList[i].itemCNT - 1 > k)
              j++
          }

          //내가 추가한 부분
          // var asdf = this.menuList[i]
          for(k = 0; k < this.menuList[i].itemCNT; k++){
            temp.push({
              checked: this.menuList[i].checked, 
              dcAmt: this.menuList[i].dcAmt,
              dcName: this.menuList[i].dcName,
              itemAmt: this.menuList[i].itemAmt,
              itemCNT: 1,
              menuName: this.menuList[i].menuName,
              menuNo: this.menuList[i].menuNo,
              orderMemo: this.menuList[i].orderMemo, 
              orderNo: this.menuList[i].orderNo,
              page: this.menuList[i].page, 
              payYN: false
            })
          }
        } else {
          detailMenu[j] = this.menuList[i];

          temp.push(this.menuList[i])
        }
      }

      this.menuList = temp
      //checked, payYn, itemCNT 초기화 
      detailMenu.forEach(element => {
        element["checked"] = false;
        element["payYN"] = false;
        // element.itemCNT = 1;
      });
    }
    else {
      this.sales.order.getDetail(this.orderNo)
        .subscribe(
        data => {
          if (!this.prepayment && data.json().length == 0) {
            this.toast('주문 내역을 가지고 올 수 없습니다.');
            this.clearAllNavCtrl();
          }
          else {
            this.menuList = data.json().list;
            var detailMenu = [];
            var i, j, k;
            //메뉴들 개수 별로 다 나눠버리기 
            for (i = 0, j = 0; this.menuList.length > i; i++ , j++) {
              if (this.menuList[i].itemCNT > 1) {
                for (k = 0; this.menuList[i].itemCNT > k; k++) {
                  detailMenu[j] = this.menuList[i];
                  if (this.menuList[i].itemCNT - 1 > k)
                    j++
                }
              } else {
                detailMenu[j] = this.menuList[i];
              }
            }
            //checked, payYn, itemCNT 초기화 
            detailMenu.forEach(element => {
              element["checked"] = false;
              element["payYN"] = false;
              element.itemCNT = 1;
            });
            this.menuList = JSON.parse(JSON.stringify(detailMenu));
          }
        },
        err => {
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('오류입니다. 다시시도해 주세요.');
        }
        );
    }
  }

  // 한번에 결제
  onegoPay() {
    if (!this.byPerson.every(isNoPartiallypaid) || !this.menuList.every(isNoPartiallypaid)) {
      this.toast('이미 결제가 진행되어 한번에 결제로 전환할 수 없습니다.');
      return 0;
    }
    this.onego = true;
    this.divide = false;
    this.payBlur = false;

    function isNoPartiallypaid(element) {
      // console.log(element.payYN == false)
      return element.payYN == false;
    }

    this.selectPayType('card')
  }

  // 나눠서 결제
  dividePay() {
    this.checkAll = false
    this.selectedAll(false)

    this.onego = false;
    this.divide = true;
    this.payBlur = true;
  }

  //외상 결제 
  creditPay() {
    let modal = this.modalCtrl.create('ConfirmAlertPage', { title: '외상 결제', label_big: '현재 주문을 외상처리 하시겠습니까?', label_small: '취소하시면 계속해서 결제 하실 수 있습니다.', type: 0 }, { cssClass: "message-modal" });
    modal.onDidDismiss((data) => {
      console.log(data);
      if (data.res == true) {
        this.pay.postCredit(this.orderNo)
          .subscribe(
          data => {
            this.toast('외상 처리 되었습니다.');
            this.clearAllNavCtrl();
            console.log(data);
          },
          err => {
            if (err == "network")
              this.toast('인터넷 연결을 확인해 주세요.');
            else
              this.toast('오류입니다. 다시시도해 주세요.');
          });
      }
    });
    modal.present();
  }

  selectedivideBy(val) {
    if (val) {
      //상픔별 클릭시   //인원별 -> 상품별 
      if (!this.byPerson.every(isNoPartiallypaid)) {
        this.toast('이미 결제가 진행되어 상품별로 전환할 수 없습니다.');
        return 0;
      }
    } else {
      //인원별 클릭시  //상품별 -> 인원별 
      if (!this.menuList.every(isNoPartiallypaid)) {
        this.toast('이미 결제가 진행되어 인원별로 전환할 수 없습니다.');
        return 0;
      }
    }
    this.divideBy = val;

    function isNoPartiallypaid(element) {
      return element.payYN == false;
    }
  }

  waitTerminal() {
    //카드 입력을 기다리는 중입니다.
    this.events.subscribe('pay:cardInfo', (data) => {
      this.cardPayInfo = data;
    });
    let modal = this.modalCtrl.create('ConfirmAlert2Page', 
      { title: 'IC 카드 삽입', 
        label_big     : 'IC 카드를 삽입해 주세요.', 
        label_payType : '카드', 
        label_payAmt  : this.dividePayTotal, 
        installmentMm : this.cardInstallmentMm, 
        orderList     : this.orderList,
        sumPrice      : this.sumPrice,
        isPay         : true },
      {
        cssClass      : 'message-modal'
      });
    modal.onDidDismiss(data => {
      if (data && data.res == true) { //  this.viewCtrl.dismiss({ res: true });이게 안대서 휴 
        console.log('111111111')
        this.payCard();
        return;
      }
      if (this.cardPayInfo.terminalYn) {
        console.log('222222222')
        if (this.cardPayInfo.isCompleted)
          this.payCard();
        this.cardPayInfo.terminalYn = false;
        this.cardPayInfo.isCompleted = false;
      }
      else {
        console.log('333333333')
        this.toast('취소되었습니다.');
        this.cardPayInfo.terminalYn = false;
        this.cardPayInfo.isCompleted = false;
      }

    });
    modal.present();
  }

  //멤버십 결제 하기 
  getMembershipData(payInfo) {
    if (payInfo.saveOrNot == true) {
      this.usePoint = Number(payInfo.pointAmt);
      this.useMileage = Number(payInfo.mileageAmt);
      this.userPhone = payInfo.barcode;
      this.userNo = payInfo.userNo;
      this.userCurPoint = Number(payInfo.curPoint);
      this.dividePayTotal -= (Number(this.usePoint) + Number(this.useMileage));
    }
    this.saveOrNot = false;
    this.showBackdrop(false);
  }

  //결제 하기 
  payCash() {
    if(this.payCashClicked)
      return
    else
      this.payCashClicked = false

    //현금 쓰기
    // var payTax = Math.floor(this.dividePayTotal / this.taxRatio * 0.1);
    if(this.prepayment){
      if (this.cashReceived < this.dividePayTotal) {
        //받은 금액이 결제 금액보다 적을때 안되게끔 
        this.toast('받은 금액이 결제금액보다 적습니다. 확인해주세요.');
        return 0;
      }
      this.pay.postCash_prepayment(
        this.navParams.get('input'),
        this.navParams.get('details'), //detail
        // this.menuList, //detail
        (this.dividePayTotal + this.usePoint + this.useMileage),//총 주문 금액 
        this.dividePayTotal,//cashAmt
        this.useMileage,//mileageAmt
        this.usePoint,//pointAmt
        this.cashPrintNo,//cashPrint
        this.userPhone,//barcode
        this.userNo,//userNo
        this.userCurPoint//curPoint
      )
        .subscribe(
        data => {
          this.toast('현금 결제 성공!');
          this.remainingAmt -= (this.dividePayTotal + this.usePoint + this.useMileage);
          this.userInfoClear();
          this.paidCheck();
          this.payBlur = true;
          this.events.publish('layout:header', '결제', false, false, false);
          if (this.remainingAmt <= 0)
            this.payComplete();
        },
        err => {
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('현금 결제 실패! 다시 시도해 주세요');
        }
        );
      this.cashPrintNo = null;
    }
    else{
      if (this.cashReceived < this.dividePayTotal) {
        //받은 금액이 결제 금액보다 적을때 안되게끔 
        this.toast('받은 금액이 결제금액보다 적습니다. 확인해주세요.');
        return 0;
      }
      this.pay.postCash(
        this.orderNo, //주문 번호 
        (this.dividePayTotal + this.usePoint + this.useMileage),//총 주문 금액 
        this.dividePayTotal,//cashAmt
        this.useMileage,//mileageAmt
        this.usePoint,//pointAmt
        this.cashPrintNo,//cashPrint
        this.userPhone,//barcode
        this.userNo,//userNo
        this.userCurPoint//curPoint
      )
        .subscribe(
        data => {
          this.toast('현금 결제 성공!');
          this.remainingAmt -= (this.dividePayTotal + this.usePoint + this.useMileage);
          this.userInfoClear();
          this.paidCheck();
          this.payBlur = true;
          this.events.publish('layout:header', '결제', false, false, false);
          if (this.remainingAmt <= 0)
            this.payComplete();
        },
        err => {
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('현금 결제 실패! 다시 시도해 주세요');
        }
        );
      this.cashPrintNo = null;
    }

    
  }

  payCard() {
    console.log('this.cardPayInfo');
    console.log(this.cardPayInfo);

    // type- 0: 신용, 1: 현금, 2: 은련
    // amt- 금액
    // instalment- 할부개월
    // this.catProtocolProvider.requestPay(0, this.sumPrice, 1, this.orderList)

    if(this.prepayment){
      //카드 쓰기 
      // var payTax = Math.floor(this.dividePayTotal / this.taxRatio * 0.1);
      this.pay.postCard_prepayment(
        this.navParams.get('input'),
        this.navParams.get('details'),
        // this.menuList, //detail
        (this.dividePayTotal + this.usePoint + this.useMileage),//총 주문 금액 
        this.dividePayTotal, //cardAmt
        this.useMileage,//mileageAmt
        this.usePoint,//pointAmt
        this.cardPayInfo.cardID,
        this.cardPayInfo.cardType,
        this.cardPayInfo.cardName,
        this.cardPayInfo.cardCompany,
        this.cardInstallmentMm,
        this.cardPayInfo.cardYYMM,
        this.cardPayInfo.approvalAmt,
        this.cardPayInfo.approvalNo,
        this.cardPayInfo.terminalStoreID,
        this.cardPayInfo.terminalID,
        this.cardPayInfo.tranID,
        this.userPhone,//barcode
        this.userNo,//userNo
        this.userCurPoint,//curPoint
      )
        .subscribe(
        data => {
          // this.toast('카드 결제 성공!');
          this.remainingAmt -= (this.dividePayTotal + this.usePoint + this.useMileage);
          this.paidCheck();
          this.userInfoClear();
          this.payBlur = true;
          this.events.publish('layout:header', '결제', false, false, false);

          if (this.remainingAmt <= 0)
            this.payComplete();
          else {
            let smallmodal = this.modalCtrl.create('MessageAlertPage', { title: '카드 결제 완료', message: '감사합니다.', color: 'gray', duration: 0.5 }, { cssClass: "message-modal" });
            smallmodal.present();
          }
        },
        err => {
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('카드 결제 실패!! 다시 시도해 주세요.');
        }
        );
    }
    else{
      //카드 쓰기 
      // var payTax = Math.floor(this.dividePayTotal / this.taxRatio * 0.1);
      this.pay.postCard(
        this.orderNo,//주문 번호
        (this.dividePayTotal + this.usePoint + this.useMileage),//총 주문 금액 
        this.dividePayTotal, //cardAmt
        this.useMileage,//mileageAmt
        this.usePoint,//pointAmt
        this.cardPayInfo.cardID,
        this.cardPayInfo.cardType,
        this.cardPayInfo.cardName,
        this.cardPayInfo.cardCompany,
        this.cardInstallmentMm,
        this.cardPayInfo.cardYYMM,
        this.cardPayInfo.approvalAmt,
        this.cardPayInfo.approvalNo,
        this.cardPayInfo.terminalStoreID,
        this.cardPayInfo.terminalID,
        this.cardPayInfo.tranID,
        this.userPhone,//barcode
        this.userNo,//userNo
        this.userCurPoint,//curPoint
      )
        .subscribe(
        data => {
          // this.toast('카드 결제 성공!');
          this.remainingAmt -= (this.dividePayTotal + this.usePoint + this.useMileage);
          this.paidCheck();
          this.userInfoClear();
          this.payBlur = true;
          this.events.publish('layout:header', '결제', false, false, false);

          if (this.remainingAmt <= 0)
            this.payComplete();
          else {
            let smallmodal = this.modalCtrl.create('MessageAlertPage', { title: '카드 결제 완료', message: '감사합니다.', color: 'gray', duration: 0.5 }, { cssClass: "message-modal" });
            smallmodal.present();
          }
        },
        err => {
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('카드 결제 실패! 다시 시도해 주세요.');
        }
        );
    }

    
  }

  terminalCheck() {

  }

  userInfoClear() {
    this.userPhone = '';
    this.userNo = null;
    this.userCurPoint = 0;
    this.usePoint = 0;
    this.useMileage = 0;
    this.useStamp = 0;
    this.byMenu.orderAmt = 0;
    this.byMenu.payTotal = 0;
  }

  //나눠서 결제시 결제 된 아이들은 disable 할수 있도록 ! 
  paidCheck() {
    if (this.divideBy) {
      this.menuList.forEach((element) => {
        if (element.checked) {
          element.checked = !element.checked;
          element.payYN = true;
        }
      });
    }
    else {
      this.byPerson[this.byPersonIdx].payYN = true;
    }
  }

  //각 계산시 계산되어야할 금액을 표시해준다. 한번에 결제이던, 나눠서 결제이던 주문 금액의 총 합은 divideOrderAmt,
  // 승인 금액의 총 합은 devidePaytotal에 입력이 된다.
  selectPayType(paytype: string, onego = true, item = null, idx = null) {

    this.payType['card'] = false;
    this.payType['cash'] = false;
    this.payType[paytype] = true;
    this.byPersonIdx = idx;
    this.payBlur = false;
    if (!onego && !idx) {
      // console.log(onego);
      // console.log(idx);
    }

    if (onego) {
      this.divideOrderAmt = this.orderAmt;
      this.cashReceived = this.dividePayTotal = this.payTotal;
      if (this.usePoint || this.useMileage)
        this.cashReceived = this.dividePayTotal -= (Number(this.usePoint) + Number(this.useMileage));
    } else {
      this.divideOrderAmt = item.orderAmt;
      this.cashReceived = this.dividePayTotal = item.payTotal;
      if (this.usePoint || this.useMileage)
        this.cashReceived = this.dividePayTotal -= (Number(this.usePoint) + Number(this.useMileage));
    }

    // if (this.prepayment)
    //   this.prepaymentPrice(true)
  }

  //카드 계산시 할부기간 계산 0개월은 일시불로 표현되며 0개월 이상의 개월은 숫자로만 표현된다
  calInstallment(i) {
    if (i == -1)
      if (this.cardInstallmentMm <= 0)
        console.log("0이하는 엄쪙 ");
      else {
        this.cardInstallmentMm = Number(this.cardInstallmentMm) - 1;
      }
    else {
      this.cardInstallmentMm = Number(this.cardInstallmentMm) + 1;
    }
  }

  //현금 계산시 잔돈 계산
  calcashReceived(i) {
    if (this.cashReceived == this.dividePayTotal)
      this.cashReceived = 0;

    if (i == 0)
      this.cashReceived = 0;
    else {
      this.cashReceived = Number(this.cashReceived) + i;
    }
  }

  //인원별 결제시 각각 인원의 합산금액과 승인금액을 계산한다. 
  calByPerson(i) {
    let calOrderAmt = this.orderAmt;
    let calPayTotal = this.payTotal;
    let k, j;
    let payCheck = false;
    for (j = 0; this.byPerson.length > j; j++) {
      if (this.byPerson[j].payYN == true) {
        payCheck = true;
        break;
      }
    }
    if (!payCheck) {
      if (i == -1)
        if (this.byPerson.length <= 1)
          console.log("1명이하는 존재하지 않습니다");
        else {
          this.byPerson.pop();
        }
      else {
        this.byPerson.push({ orderAmt: this.orderAmt, payTotal: this.payTotal, payYN: false });
      }
      for (k = 0; this.byPerson.length > k; k++) {
        if ((this.byPerson.length - 1) == k) {
          this.byPerson[k].orderAmt = calOrderAmt;
          this.byPerson[k].payTotal = calPayTotal;
        } else {
          this.byPerson[k].orderAmt = Math.ceil((calOrderAmt / (this.byPerson.length - k)) / this.truncValue) * this.truncValue;
          calOrderAmt = calOrderAmt - this.byPerson[k].orderAmt;
          this.byPerson[k].payTotal = Math.ceil((calPayTotal / (this.byPerson.length - k)) / this.truncValue) * this.truncValue;
          calPayTotal = calPayTotal - this.byPerson[k].payTotal;
        }
      }
    } else {
      this.toast('결제 내역이 있어서 인원을 조정할 수 없습니다.');
    }
  }

  //메뉴별 계산시 계산할 메뉴들을 선택하여 체크에 표시가 되게 한다. 
  selectOrder(idx) {
    // console.log(this.menuList)
    if (!this.menuList[idx].checked) {
      this.checkAll = false;
      // console.log('false!')
    }
    else {
      // console.log('true')
    }
    this.menuList[idx].checked = !this.menuList[idx].checked;
    // console.log('selectOrder')
    this.orderSum();

    this.payBlur = true
    for (let i = 0; i < this.menuList.length; i++) {
      if (this.menuList[i].checked)
        this.payBlur = false
    }
  }

  //메뉴 계산시 전체 메뉴를 선택하여 체크에 표시가 되게 한다. 
  selectedAll(setUnchecked = null) {
    //하나라도 체크된 게 있으면 참
    var anythingChecked = false

    for(let i = 0; i < this.menuList.length; i++){
      if(this.menuList[i].checked == true){
        anythingChecked = true
        break
      }
    }

    for(let i = 0; i < this.menuList.length; i++){
      this.menuList[i].checked = anythingChecked ? (setUnchecked == false ? false : true): false
    }

    // for (var i = 0; this.menuList.length > i; i++) {
    //   if (this.menuList[i].payYN == false)
    //     this.menuList[i].checked = this.checkAll;
    // }
    this.orderSum();
  }

  //메뉴별 계산시 선택된 메뉴들만 sum하여 합산 금액과 승인금액을 계산한다. . 
  orderSum() {
    let calOrderAmt: number = 0;
    let calPayTotal: number = 0;
    this.byMenu.orderAmt = 0;
    this.byMenu.payTotal = 0;
    for (var i = 0; this.menuList.length > i; i++) {
      if (this.menuList[i].checked) {
        //지금 이거 갯수 문제 해결하면 롤백해야 할 수도 있다....
        calOrderAmt += this.menuList[i].itemAmt * this.menuList[i].itemCNT;
        calPayTotal += this.menuList[i].itemAmt - this.menuList[i].dcAmt;
        this.byMenu.orderAmt = calOrderAmt;
        this.byMenu.payTotal = calPayTotal;
      }
    }

    //QA때문에... 추가 합니다.. 원래는 카드/현금 버튼 눌러야 반영된느데 .. ㅜㅜ 
    if (this.payType.card)
      this.selectPayType('card', false, this.byMenu)
    else
      this.selectPayType('cash', false, this.byMenu)
  }

  //현금 영수증 선택 시 
  cashPrint() {
    let modal = this.modalCtrl.create('CashPrintPage', {}, { cssClass: 'mileage-modal', enableBackdropDismiss: false });
    modal.onDidDismiss(data => {
      if (data) {
        this.cashPrintNo = data;
        this.payCash();
      } else {
        this.toast('현금 영수증이 취소되었습니다.');
      }
    });
    modal.present();
  }
  // memebershipClose(){

  // }

  //모든 결제 완료시 
  payComplete(closeAll = false) {
    let modal = this.modalCtrl.create('MessageAlertPage', { title: '결제 완료', message: '결제가 완료되었습니다!', color: 'gray' }, { cssClass: "message-modal" });
    modal.onDidDismiss(() => {
      this.clearAllNavCtrl();
    });
    modal.present();
  }

  payClear() {
    this.divideOrderAmt = 0;
    this.dividePayTotal = 0;
    this.cashReceived = 0;
    this.cardInstallmentMm = 0;
    this.cashPrintNo = null;
    this.useMileage = null;
    this.useStamp = null;
    this.usePoint = null;
    this.userCurPoint = null;
    this.userNo = null;
    this.userPhone = "";
  }

  clearAllNavCtrl() {

    if (this.remain > 0)
      this.navCtrl.pop();
    else
      this.events.publish('nav:go', 0);

  }

  toast(text) {
    if (this.toastInstance) {
      return;
    }
    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
      cssClass: 'custom-toast',
    });
    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });
    this.toastInstance.present();
  }

  prepaymentPrice(isPrepayment: boolean) {
    if (isPrepayment) {
      this.orderAmt = 0
      this.discounted = 0
      this.payTotal = 0

      for (let i = 0; i < this.orderInfo.length; i++) {
        let menu = this.orderInfo[i]

        this.orderAmt += menu.itemAmt * menu.itemCNT
        this.discounted += menu.dcAmt * menu.itemCNT
      }

      this.divideOrderAmt = this.orderAmt
      this.payTotal = this.dividePayTotal = this.divideOrderAmt - this.discounted

      // console.log('this.orderAmt, this.divideOrderAmt: ' + this.orderAmt)
      // console.log('this.payTotal, this.dividePayTotal: ' + this.payTotal)
    }
    else
      return
  }
}

