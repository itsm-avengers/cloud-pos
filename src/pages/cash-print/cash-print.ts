import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, ModalController, Toast } from 'ionic-angular';
import { Pay } from '../../services/pay';

@IonicPage()
@Component({
  selector: 'page-cash-print',
  templateUrl: 'cash-print.html',
})
export class CashPrintPage {
  @ViewChild('phonenum') phoneInput;
  @ViewChild('corpnum') corpInput;

  title: string = "현금영수증 발급";
  modified: boolean = false;
  data: any = { payNo: '', orderNo: '' };
  personal: boolean = true;
  phoneNum: string = "";

  private toastInstance: Toast;
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private pay: Pay, public toastCtrl: ToastController, public modalCtrl: ModalController) {
  }

  ionViewDidLoad() {
    if (this.navParams.get('modified')) {
      this.modified = this.navParams.get('modified');
      this.data = this.navParams.get('data');
    }
  }
  checkType(): boolean {
    if (this.personal) {
      this.phoneInput.focus();
      this.phoneNum = "010";
    }
    else {
      this.phoneNum = "";
      this.corpInput.focus();
    }
    return this.personal;
  }

  ionViewDidEnter() {
    this.phoneInput.focus();
  }

  modifyCashPrint(): boolean {
    let tf = false;
    //단말변경
    var cashPrint = Math.ceil(Math.random() * 100000);
    this.pay.putCash(this.data.payNo, cashPrint)
      .subscribe(
        data => {
          tf = true;
          console.log(data.json());
          this.toast('현금 영수증 처리 되었습니다.');

        },
        err => {
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else if (err.json().message == "이미 현금영수증이 발행되었습니다.")
            this.toast('이미 현금영수증 처리된 결제 입니다.');
          else
            this.toast('오류입니다. 다시시도해 주세요.');
        }
      );
    return tf;

  }

  close(tf) {
    console.log(this.modified + '5454646556565');
    if (tf) {
      if (this.phoneNum == "") {
        this.toast('번호를 입력해 주세요.');
        return;
      }
      if (this.modified) {
        this.modifyCashPrint();
        this.viewCtrl.dismiss(tf);
        // if(this.modifyCashPrint()){
        //   //왜인지 아얘 들어오지 않기 때문에 이 코드는 잠시 보류 
        //   this.viewCtrl.dismiss(tf);
        //   setTimeout(() => {
        //     let modal = this.modalCtrl.create(MessageAlertPage, { title: '현금영수증 발행 완료', message: '개인번호 ' + this.phoneNum, color: 'gray' }, { cssClass: "message-modal" });
        //     modal.present();
        //   }, 500);
        // }else{
        //   this.viewCtrl.dismiss(tf);
        // }
      }
      else {
        //단말변경
        var cashPrint = Math.ceil(Math.random() * 100000);
        this.viewCtrl.dismiss(cashPrint);
      }
    }

    else {
      this.viewCtrl.dismiss();
    }
  }

  toast(text) {

    if (this.toastInstance) {
      return;
    }

    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
    });

    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });

    this.toastInstance.present();
  }

}
