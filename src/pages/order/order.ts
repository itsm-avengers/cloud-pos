import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Events, ModalController, Scroll, IonicPage, Toast, ToastController, Platform } from 'ionic-angular';
// import { Sales } from '../../services/sales';
import * as _ from 'lodash';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';
import { NetworkStatusService } from '../../services/network-status.service';
import { Setting } from '../../services/setting';
import { Order, OrderTablePage } from '../order-table/order-table';
import { OrderMenuListPage } from '../order-menu-list/order-menu-list';
import { Device } from '@ionic-native/device';
import { OrderDcPage } from '../order-dc/order-dc';
import { OrderListPage } from '../order-list/order-list';
import { LoginSession } from '../../services/login.session';
import { TranslateService } from '@ngx-translate/core';
import { User } from '../../services/user';
import { PACKAGE_NAME, IS_GENIE } from '../../config';
import { CatProtocolProvider } from '../../providers/cat-protocol/cat-protocol';

//declare const GigagenieApi;
// declare const chrome;

const DeleteOrder = gql`
  mutation DeleteOrder($orderNo: Int!) {
    deleteOrder(orderNo: $orderNo)
  }
`;

const AddOrder = gql`
mutation AddOrder($input: OrderInput!, $details: [OrderDetailInput], $deviceId: String) {
  addOrder(input: $input, details: $details) {
    id
    total
    dcTotal
    detail {
      id
      menuNo
      menuName
      diff
      itemCNT
      itemAmt
      stockCNT
      taxType
      dcAmt
      dcName
    }
    error {
      key
      value
    }
    tableId
  }
}
`;


@IonicPage()
@Component({
  selector: 'page-order',
  templateUrl: 'order.html'
})
export class OrderPage {
  @ViewChild('nav') nav: NavController;
  @ViewChild('menuList') menuList: Scroll;
  // @ViewChild('selectedTableId') selectedTableId: any
  // @ViewChild('prepayment') prepayment: any

  selectedTableId: any

  rootPage = 'OrderTablePage';

  order: any = { id: null, detail: [] };
  meta: any = null;

  watingNo: number = -1;
  bOnline: boolean = true;

  save: boolean = true;

  checkAll: boolean = false;
  editMode: boolean = false;

  filterHeight: string = "104px";
  floatEnabled: boolean = false;

  isNewOrder: boolean = true;

  dcItem: any = {}; 

  //주문목록 오른쪽 테이블 태그
  prepaymentFloor = {
    dispOrder: 0,
    id: 2929,
    name: "선불층",
    __typename: "Table"
  }
  tableTags = [
    {
      dispOrder: 0,
      dispShape: "0",
      floorId: -1,
      id: 1,
      name: "1",
      __typename: "Table",
      detail: [],
      selected: true
    },
    {
      dispOrder: 1,
      dispShape: "0",
      floorId: -1,
      id: 2,
      name: "2",
      __typename: "Table",
      detail: [],
      selected: false
    },
    {
      dispOrder: 2,
      dispShape: "1",
      floorId: -1,
      id: 3,
      name: "3",
      __typename: "Table",
      detail: [],
      selected: false
    },
  ]
  selectedPrepayment = 0 //선불 테이블 중 현재 선택된 테이블 인덱스

  //선불 여부 편하게 따지려고 만든 것
  prepayment: boolean = this.loginSession.getInfo().posType == 1
  
  private toastInstance: Toast;

  constructor(public navCtrl: NavController, public navParams: NavParams, private events: Events, private apollo: Apollo, public networkStatus: NetworkStatusService, private modal: ModalController, private setting: Setting, private device: Device, private toastCtrl: ToastController,private loginSession: LoginSession, private trans: TranslateService, private user: User, private platform: Platform, public cat: CatProtocolProvider) {
    networkStatus.networkStatus$.subscribe(res => {
      this.bOnline = res;
    });

    console.log(this.rootPage)
    this.rootPage = !this.prepayment ? 'OrderTablePage' : 'OrderMenuListPage'
    console.log(this.rootPage)
  }

  ionViewWillEnter() {

    this.events.subscribe('layout:backdrop', (tf) => {
      this.editMode = tf;
      this.floatEnabled = false;
    });
    this.events.subscribe('layout:menu-filter-height', (contentHeight) => {
      this.filterHeight = contentHeight + 5 + 'px';
    });
    this.events.subscribe('nav:goToMenu', () => {
      this.goToMenu();
    });

    // 테이블 클릭했을 때, 층-테이블-주문 정보를 가져옴.
    this.events.subscribe('order:get', (order, meta) => {
      this.order = _.cloneDeep(order);
      this.meta = meta;
      if (!order) {
        this.order = {
          id: null,
          detail: []
        };
        this.isNewOrder = true;
      }
      else {
        //this.order.id=1;
        this.order.detail.forEach(element => {
          if (element.stockCNT != -1)
            element.stockCNT += element.itemCNT;
        });
        this.isNewOrder = false;
      }
    });

    // 메뉴 클릭 시 주문 리스트에 추가함
    this.events.subscribe('order:add', (item, opt) => {
      this.addOrder(item);
    });

    // 할인율 적용
    this.events.subscribe('dc:apply', (item) => {
      this.applyDc(item);
    });
    this.events.subscribe('nav:goToTable', () => {
      if (this.nav && this.nav.getActive() && this.nav.getActive().component != OrderTablePage) {
        if(this.prepayment)
          this.nav.setRoot('OrderMenuListPage');
        else
          this.nav.setRoot('OrderTablePage');
        this.events.publish('order:get', null);
      }
    });

    this.events.publish('layout:header-background', '50px');
    this.events.publish('nav:index', 0);
    this.events.publish('layout:header', '영업', true, false, true);

    // this.events.publish('order:passPrepaymentId', this.tableTags)
    // this.clickTag(0)
  }

  ionViewWillLeave() {
    // if (this.save) {
    //   var detail = this.getDetail();
    //   if (detail.length > 0)
    //     this.saveOrder(detail).subscribe(()=>{}, (error)=>{
    //       this.networkStatus.addQueue({
    //         time: new Date().getTime() / 1000 | 0,
    //         order: {
    //             orderNo: this.order.id,
    //             orderName: this.floor.table[this.tableIndex].name,
    //             orderType: '0',
    //             waitingNo: -1,
    //             tableNo: this.floor.table[this.tableIndex].id
    //         },
    //         detail: detail
    //       });
    //     });
    // }
    // this.events.unsubscribe('layout:backdrop');
    this.events.unsubscribe('layout:menu-filter-height');
    this.events.unsubscribe('order:get');
    this.events.unsubscribe('order:add');
    this.events.unsubscribe('dc:apply');
    this.events.unsubscribe('nav:goToMenu');
    this.events.unsubscribe('nav:goToTable');
        // console.log(this.tableTags[this.selectedPrepayment]['detail'])
  }

  selectOrder(idx) {
    if(this.prepayment){
      if(!this.tableTags[this.selectedPrepayment]['detail'][idx].checked)
        this.checkAll = false;
    }
    else{
      if(!this.order.detail[idx].checked)
        this.checkAll = false;
    }

    if (this.dcItem.dcType != null) {
      this.calcDc(idx);
      this.events.publish('dc:select-init');
      this.dcItem = {};

      for(let i = 0 ; i < this.tableTags[this.selectedPrepayment]['detail'].length; i++)
        this.tableTags[this.selectedPrepayment]['detail'][i].checked = false

      return;
    }
  }

  selectedAll() {
    if(this.prepayment && this.tableTags[this.selectedPrepayment]['detail']) {
      this.tableTags[this.selectedPrepayment]['detail'].forEach((element, index) => {
        element.checked = this.checkAll;
        if (this.dcItem.dcType != null)
          this.calcDc(index);
      });
    }
    else if(!this.prepayment && this.order.detail) {
      this.order.detail.forEach((element, index) => {
        element.checked = this.checkAll;
        if (this.dcItem.dcType != null)
          this.calcDc(index);
      });
    }

    this.events.publish('dc:select-init');
    this.dcItem = {};
  }

  applyDc(item) {
    this.dcItem = item;
  }

  calcDc(idx) {

    if(this.prepayment){
      this.tableTags[this.selectedPrepayment]['detail'][idx].dcName = this.dcItem.dcName;
      if (this.dcItem.dcType == 2)
        this.tableTags[this.selectedPrepayment]['detail'][idx].dcAmt = this.tableTags[this.selectedPrepayment]['detail'][idx].itemAmt * (this.dcItem.dcAmt * 0.01);
      else if (this.dcItem.dcType == 1) 
        this.tableTags[this.selectedPrepayment]['detail'][idx].dcAmt = this.dcItem.dcAmt
      if(this.tableTags[this.selectedPrepayment]['detail'][idx].dcAmt > this.tableTags[this.selectedPrepayment]['detail'][idx].itemAmt)
        this.tableTags[this.selectedPrepayment]['detail'][idx].dcAmt = this.tableTags[this.selectedPrepayment]['detail'][idx].itemAmt
      this.isNewOrder = true;
      this.tableTags[this.selectedPrepayment]['detail']
    }
    else{
      this.order.detail[idx].dcName = this.dcItem.dcName;
      if (this.dcItem.dcType == 2)
        this.order.detail[idx].dcAmt = this.order.detail[idx].itemAmt * (this.dcItem.dcAmt * 0.01);
      else if (this.dcItem.dcType == 1) 
        this.order.detail[idx].dcAmt = this.dcItem.dcAmt
      if(this.order.detail[idx].dcAmt > this.order.detail[idx].itemAmt)
        this.order.detail[idx].dcAmt = this.order.detail[idx].itemAmt
      this.isNewOrder = true;
    }

  }

  getDetail() {
    if(this.loginSession.getInfo().posType == 1){
      if(this.tableTags[this.selectedPrepayment] && this.tableTags[this.selectedPrepayment]['detail'])
        return this.tableTags[this.selectedPrepayment]['detail'].map(value => {
          return{
            menuNo: value.menuNo,
            menuName: value.menuName,
            itemAmt: value.itemAmt,
            itemCNT: value.itemCNT,
            diff: value.diff,
            dcName: value.dcName,
            dcAmt: value.dcAmt,
            taxType: value.taxType }; }); }
    else{
      if(this.order && this.order.detail)
        return this.order.detail.map(value => {
          return{
            menuNo: value.menuNo,
            menuName: value.menuName,
            itemAmt: value.itemAmt,
            itemCNT: value.itemCNT,
            diff: value.diff,
            dcName: value.dcName,
            dcAmt: value.dcAmt,
            taxType: value.taxType }; }); }
  }

  saveOrder(details) {
    if(((this.prepayment && !this.tableTags[this.selectedPrepayment]['id']) || 
        (!this.prepayment && !this.order.id)) && details.length == 0) {
      this.toast('주문을 추가해주세요.');
      return ;
    }
    
    if (details.length == 0) {
      let modal = this.modal.create('ConfirmAlertPage', { title: '주문 취소', label_small: '주문 메뉴 전체를 삭제하시겠습니까?', type: 1 }, { cssClass: 'message-modal', enableBackdropDismiss: false });
      modal.onDidDismiss(data => {
        this.cancelOrder();
      });
      modal.present();

      return;
    } 
    else {
      var vars = {
        input: {
          orderNo: this.order.id,
          orderName: this.meta.table.name,
          orderType: '1',
          floorNo: this.meta.floor.id,
          waitingNo: -1,
          tables: this.meta.table.id,
        },
        details: details,
        deviceId: this.device.uuid
      };

      return this.apollo.mutate({
        mutation: AddOrder,
        variables: vars,
        update: (store, { data: { addOrder } }) => {
          const data: any = store.readQuery({ query: Order });
          let order = data.getStore.order.find(_ => _.id == addOrder.id)
          if (order) {
            order = addOrder;
          } else {
            data.getStore.order.push(addOrder);
          }
          // Write our data back to the cache.
          store.writeQuery({ query: Order, data });
        }
      });
    }
  }

  saveNpop() {
    let tmp = this.saveOrder(this.getDetail());

    if (tmp) {
      tmp.subscribe(
        () => {
          this.events.publish('order:get', null);
          if(this.prepayment){
            this.rootPage = 'OrderMenuListPage';
            this.nav.setRoot('OrderMenuListPage', {}, { animate: false });
          }
          else{
            this.rootPage = 'OrderTablePage';
            this.nav.setRoot('OrderTablePage', {}, { animate: false });
          }
        },
        error => {
          console.log('주문저장실패');
        }
      );
    }
  }

  addOrder(item) {
    //전영찬: 선불 여부 따라서 새 메뉴를 추가할 위치를 달리한다
    if(this.loginSession.getInfo().posType == 1){
      for (var i = 0; i < this.tableTags[this.selectedPrepayment]['detail'].length; i++) {
        if (this.tableTags[this.selectedPrepayment]['detail'][i].menuNo == item.menuNo) {
          if ((item.stockSaleYN == 1 || this.tableTags[this.selectedPrepayment]['detail'][i].stockCNT != -1) && 
            this.tableTags[this.selectedPrepayment]['detail'][i].itemCNT >= this.tableTags[this.selectedPrepayment]['detail'][i].stockCNT){
            return;}
          else {
            this.itemPlus(i);
            return;}}}

      this.tableTags[this.selectedPrepayment]['detail'].push({
        menuNo: item.menuNo,
        menuName: item.name.ko,//여긴 잘 바꿨다!
        // menuName: item.menuName,
        itemCNT: 1,
        itemAmt: item.saleAmt,
        diff: -1,
        stockCNT: item.stockSaleYN == '1' ? item.stockCNT : -1,
        dcName: "",
        dcAmt: 0,
        isNew: true,
        taxType: "" + item.taxType})}
    //여기는 후불인 경우
    else{
      for (var i = 0; i < this.order.detail.length; i++) {
        if (this.order.detail[i].menuNo == item.menuNo) {
          if ((item.stockSaleYN == 1 || this.order.detail[i].stockCNT != -1) &&
            this.order.detail[i].itemCNT >= this.order.detail[i].stockCNT) {
            return;}
          else {
            this.itemPlus(i);
            return;}}}

      this.order.detail.push({
        menuNo: item.menuNo,
        menuName: item.name.ko,
        // menuName: item.menuName,
        itemCNT: 1,
        itemAmt: item.saleAmt,
        diff: -1,
        stockCNT: item.stockSaleYN == '1' ? item.stockCNT : -1,
        dcName: "",
        dcAmt: 0,
        isNew: true,
        taxType: "" + item.taxType});}

    this.isNewOrder = true;

    setTimeout(() => {
      this.menuList._scrollContent.nativeElement.scrollTop =
        this.menuList._scrollContent.nativeElement.scrollHeight - this.menuList._scrollContent.nativeElement.clientHeight;
    }, 300);

  }

  removeOrder() {
    this.doRemove();
  }

  doRemove() {
    if(this.loginSession.getInfo().posType == 1){
      for (var i = 0; i < this.tableTags[this.selectedPrepayment]['detail'].length; i++) {
        if (this.tableTags[this.selectedPrepayment]['detail'][i].checked) {
          this.tableTags[this.selectedPrepayment]['detail'].splice(i, 1);
          i--;
          this.isNewOrder = true; } } }
    else{
      for (var i = 0; i < this.order.detail.length; i++) {
        if (this.order.detail[i].checked) {
          this.order.detail.splice(i, 1);
          i--;
          this.isNewOrder = true; } } }

    this.checkAll = false;
  }

  newTable() {
    this.events.publish('table:add');
  }

  cancelOrder() {
    if (this.bOnline) {
      this.apollo.mutate({
        mutation: DeleteOrder,
        variables: {
          orderNo: this.order.id,
          deviceId: this.device.uuid
        },
        update: (store, { data: { deleteOrder } }) => {
          const data: any = store.readQuery({ query: Order });
          if (deleteOrder) {
            data.getStore.order.splice(data.getStore.order.findIndex(_ => _.id == this.order.id), 1);
          }
          // Write our data back to the cache.
          store.writeQuery({ query: Order, data });
        }
      })
      .subscribe(
        (res) => {
          this.save = false;
          this.nav.setRoot('OrderTablePage')
        },
        (error) => {
          console.log('에러');
        }
      );
    } else {
      alert("오프라인상태에서는 주문취소를 할 수 없습니다.");
    }
  }

  itemPlus(idx) {
    if(this.loginSession.getInfo().posType == 1){
      if(this.tableTags[this.selectedPrepayment]['detail'][idx] &&
        (this.tableTags[this.selectedPrepayment]['detail'][idx].stockCNT != -1 &&
          this.tableTags[this.selectedPrepayment]['detail'][idx].itemCNT < this.tableTags[this.selectedPrepayment]['detail'][idx].stockCNT) || this.tableTags[this.selectedPrepayment]['detail'][idx].stockCNT == -1){
        this.tableTags[this.selectedPrepayment]['detail'][idx].itemCNT++;
        this.tableTags[this.selectedPrepayment]['detail'][idx].diff--;
        this.isNewOrder = true; } }
    else{
      if(this.order.detail[idx] && (this.order.detail[idx].stockCNT != -1 && this.order.detail[idx].itemCNT < this.order.detail[idx].stockCNT) || this.order.detail[idx].stockCNT == -1) {
        this.order.detail[idx].itemCNT++;
        this.order.detail[idx].diff--;
        this.isNewOrder = true; } }
  }

  itemMinus(idx) {
    if(this.loginSession.getInfo().posType == 1){
      if (this.tableTags[this.selectedPrepayment]['detail'][idx] && this.tableTags[this.selectedPrepayment]['detail'][idx].itemCNT > 1) {
        this.tableTags[this.selectedPrepayment]['detail'][idx].itemCNT--;
        this.tableTags[this.selectedPrepayment]['detail'][idx].diff++;
        this.isNewOrder = true; } }
    else{
      if (this.order.detail[idx] && this.order.detail[idx].itemCNT > 1) {
        this.order.detail[idx].itemCNT--;
        this.order.detail[idx].diff++;
        this.isNewOrder = true; } }
  }

  get orderSum(): number {
    var sum: number = 0;
    if (!this.prepayment && this.order.detail)
      this.order.detail.forEach(element => {
        sum += (element.itemCNT * (element.itemAmt - element.dcAmt));
      });
    else if(this.prepayment && this.tableTags[this.selectedPrepayment]['detail'])
      this.tableTags[this.selectedPrepayment]['detail'].forEach(element => {
        sum += (element.itemCNT * (element.itemAmt - element.dcAmt));
      });
    return sum;
  }

  goToPay() {
    var detail = this.getDetail();
    if (detail && detail.length > 0){
      if(this.prepayment){
        var input = {
          input: {// <-------------------------------------------------이게 pay.ts로 넘어감
            orderNo: this.tableTags[this.selectedPrepayment]['id'],
            orderName: this.selectedPrepayment,
            orderType: '1',
            floorNo: -1,
            waitingNo: -1,
            tables: this.selectedPrepayment,
          },
          details: detail,
          // details: this.menuList,
          deviceId: this.device.uuid
        }
        this.navCtrl.push('PayPage',
          {'orderNo': this.order.id,
            'input': input['input'],
            'details': input['details'],
          'orderInfo': this.tableTags[this.selectedPrepayment]['detail']},
          { animate: true, animation: 'ios-transition' });
      }
      else{
        this.saveOrder(detail)
        .subscribe(
          (res: any) => {
            this.order.id = res.data.addOrder.id;
            this.navCtrl.push('PayPage', { 'orderNo': this.order.id, 'details': detail }, { animate: true, animation: 'ios-transition' });
          }
        );
      }
    }
    else {
      this.toast('주문을 추가해주세요.');
    }

    // if (this.bOnline) {
    //   var detail = this.getDetail();
    //   if (detail.length > 0)
    //     this.saveOrder(detail).subscribe((res: any)=>{
    //       this.order.id = res.data.addOrder.id;
    //       this.navCtrl.setRoot(PayPage, { 'orderNo': this.floor.table[this.tableIndex].order.id });
    //     });
    //   else
    //     alert("빈 주문");
    // } else {
    //   alert("오프라인상태에서는 결제를 할 수 없습니다.");
    // }
  }

  delayOrder() {
    let modal = this.modal.create('MessageAlertPage', { title: '해당 주문 내역이 보류되었습니다.', message: '주문 보류 내역은 최초 보류시점으로 10분 초과 시 자동 삭제됩니다.' }, { cssClass: 'message-modal', enableBackdropDismiss: false });
    modal.present();
  }

  newCategory(val) {
    let modal;

    if (val == 0) {
      modal = this.modal.create('OneInputAlertPage', { title: '층 추가', label: '층명' }, { cssClass: 'message-modal', enableBackdropDismiss: false });
    } else {
      modal = this.modal.create('OneInputAlertPage', { title: '카테고리 추가', label: '카테고리명' }, { cssClass: 'message-modal', enableBackdropDismiss: false });
    }

    if (val == 0) {
      modal.onDidDismiss(data => {
        if (data && data.val.length > 0) {
          this.setting.floor.post(data.val, 0)
            .subscribe(
              data => {
                this.events.publish('reloadTable');
              },
              error => {
              }
            );
        }
      });
    }

    else {
      modal.onDidDismiss(data => {
        if (data && data.val.length > 0) {
          this.setting.menuCategory.post(data.val)
            .subscribe(
              data => {
                console.log('카테고리 추가 완료');
                this.events.publish('menu-list:refresh');
              }
            );
        }
      });
    }
    modal.present();
  }

  newMenu() {
    this.events.publish('modal:new-menu');
  }

  newSetMenu() {
    this.events.publish('modal:new-set-menu');
  }

  newDc() {
    this.events.publish('modal:new-dc');
  }

  isInTable() {
    if(this.loginSession.getInfo().posType == 1)
      return false
    if (this.nav && this.nav.getActive() && this.nav.getActive().component == OrderTablePage)
      return true;
    else
      return false;
  }

  isInOrderList() {
    if(this.loginSession.getInfo().posType == 1)
      return false
    if (this.nav && this.nav.getActive() && this.nav.getActive().component == OrderListPage)
      return true;
    else
      return false;
  }

  isInDc() {
    if (this.nav && this.nav.getActive() && this.nav.getActive().component == OrderDcPage)
      return true;
    else
      return false;
  }

  isPay() {
    var detail = this.getDetail();
    if (detail && detail.length > 0)
      return true;
    else
      return false;
  }

  goToTable() {
    this.nav.push('OrderTablePage',{}, { animate: false });
  }

  goToOrderList() {
    this.nav.push('OrderListPage',{}, { animate: false });    
  }

  goDc() {
    if(this.prepayment)
      this.tableTags[this.selectedPrepayment]['detail'].forEach((element, index) => {
        element.checked = false })

    if (this.nav.getActive().component != OrderPage){
      this.nav.push('OrderDcPage', {}, { animate: false });
    }
  }

  goToMenu() {
    if (this.meta && this.meta.table && this.meta.table.id && this.order.detail && this.nav.getActive().component != OrderMenuListPage) {
      this.nav.push('OrderMenuListPage', { orderDetail: this.order.detail }, { animate: false });
    }
    else if(this.prepayment){
      this.nav.push('OrderMenuListPage', { orderDetail: this.order.detail }, { animate: false });
    }
    else {
      this.toast('테이블을 선택해주세요.');
    }
  }

  salesAmt() {
    let modal = this.modal.create('SalesAmtModifyPage', {}, { cssClass: 'basic-modal', enableBackdropDismiss: false });
    modal.present();
  }

  toast(text) {
    if (this.toastInstance) {
      return;
    }

    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
      cssClass: 'custom-toast',
    });

    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });

    this.toastInstance.present();
  }

  //tableTags를 수정해서 테이블을 바꾸는 데에 씀 
  clickTag(index){

    for(let tag of this.tableTags)
      tag['selected'] = false

    for(let i = 0; i < this.tableTags[this.selectedPrepayment]['detail'].length; i++)
      this.tableTags[this.selectedPrepayment]['detail'][i]['isNew'] = false

    this.selectedPrepayment = index
    this.tableTags[index]['selected'] = true

    this.meta = {
      floor: {
        dispOrder: 0,
        id: -1,
        name: "선불층",
        tableCNT: this.tableTags.length
      },
      table: this.tableTags[index]
    }
  }
}
