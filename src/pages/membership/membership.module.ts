import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MembershipPage } from './membership';
import { CheckboxModule } from "../../components/check-box/check-box.module";
import { AlertHeaderComponentModule } from "../../components/alert-header/alert-header.module";
import { NumpadModule } from "../../modules/Numpad/numpad.module";
import { MileageComponentModule } from "../../components/mileage/mileage.module";
import { TablePushComponentModule } from '../../components/table-push/table-push.module';
import { TableComponentModule } from '../../components/table/table.module';
import { EditorComponentModule } from '../../components/editor/editor.module';

@NgModule({
  declarations: [
    MembershipPage,
  ],
  imports: [
    IonicPageModule.forChild(MembershipPage),
    CheckboxModule,
    AlertHeaderComponentModule,
    NumpadModule,
    MileageComponentModule,
    TablePushComponentModule,
    TableComponentModule,
    EditorComponentModule
  ],
  exports: [
    MembershipPage
  ]
})
export class MembershipPageModule {}
