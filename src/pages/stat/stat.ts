import { Component, ViewChild, ChangeDetectorRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { IonicPage, NavController, NavParams, Events, ToastController, ModalController, Toast } from 'ionic-angular';
import { TableComponent } from '../../components/table/table';
import { DatePickerModalPage } from '../date-picker-modal/date-picker-modal';
import { Log } from '../../services/log';
import { Setting } from '../../services/setting';
import { Sales } from '../../services/sales';
import { Pay } from '../../services/pay';
import * as _ from 'lodash';


@IonicPage()
@Component({
  selector: 'page-stat',
  templateUrl: 'stat.html',
})
export class StatPage {

  // @ViewChild('jpd') jpd: JustPickDateComponent;

  today = new Date();
  endDate = this.today;
  startDate = new Date(this.today.getTime() - (7 * 24 * 60 * 60 * 1000), );
  selectDate: boolean = true;
  selectedPeriod: number = 2;
  category1 = ['전체', '카드', '현금', '포항포인트', '마일리지']; //statType -> 2, 
  category2 = ['전체', '카드사']; //statType -> 0, 
  category3 = [{}] //statType -> 1, 메뉴 카테고리 받아오기 
  category4 = ['']; //statType -> 3,4,5,
  showDatePicker: boolean = false;
  showDrop: boolean = false;
  selectedCategoryIdx: number = 0;
  //stat 종류 저장 
  statType: number = 0; // 0:결제 수단별, 1: 상품별. 2: 결제 내역, 3:현금 영수증 내역, 4: 변경,취소 내역 5:마감 내역
  payType: any[] = ['', '카드', '현금', '포인트', '마일리지', '기타'];
  statFilter: number = 0;
  statData: any[];
  statTotal: number = 0;
  statSumData: any[] = [0, 0, 0, 0];
  pageNo: number = 1;
  selectedRowData: any = {};

  //카드 결제 정보 단말 연결되면 지우기 
  cardPayInfo = {
    'payNo': 0,
    'cardID': "0000-0000-0000-000",
    'cardType': "신용",
    'cardName': "삼성골드",
    'cardCompany': "삼성카드",
    'installmentMm': "0",
    'cardYYMM': "00-00",
    'approvalAmt': 0,
    'approvalNo': Math.ceil(Math.random() * 1000),
    'terminalID': "1234",
    'terminalStoreID': "181",
    'tranID': 123456
  }
  private toastInstance: Toast;

  constructor(public navCtrl: NavController, public navParams: NavParams, private events: Events, public toastCtrl: ToastController, public setting: Setting, private modal: ModalController, private log: Log, private sales: Sales, private modalCtrl: ModalController, private pay: Pay, private ref: ChangeDetectorRef) {

  }
  ionViewCanEnter() {
  }

  ionViewWillEnter() {
    this.events.publish('layout:header', '조회', true, false, true);
    this.selectedStatType(this.statType, this.pageNo);
    this.getMenuCategory();
    this.selectedRowData = {};
    this.events.subscribe('nav:goPay', (orderNo, remain) => {
      this.navCtrl.push('PayPage', { 'orderNo': orderNo, 'remain': remain }, { animate: true, animation: 'ios-transition' });
    });

  }

  ionViewDidLeave() {
    this.events.publish('layout:backdrop', false);
    this.events.unsubscribe('nav:goPay');
  }





  getMenuCategory() {
    this.setting.menuCategory.get()
      .subscribe(
        data => {
          if (data.json() == null)
            this.toast('메뉴 카테고리를 가지고 올 수 없습니다.');
          else {
            console.log(data.json());
            this.category3 = data.json();
            this.category3.unshift({ categoryName: '전체', categoryNo: 0 });
          }
        },
        err => { }
      );
  }

  //stat 버튼 클릭시 stat type이 변경되며 불러옴 
  selectedStatBt(statType) {
    //결제 수단별, 상품별, 결제 내역, 현금 영수증 내역, 마감 내역 클릭시 날짜와 filter 초기화 
    this.statType = statType;
    this.startDate = new Date(this.today.getTime() - (7 * 24 * 60 * 60 * 1000), );
    this.endDate = this.today;
    this.selectedPeriod = 2;
    this.statFilter = 0;
    this.pageNo = 1;
    this.selectedCategoryIdx = 0;
    this.statTotal = 0;
    this.selectedStatType(statType, 1);

  }

  selectedStatType(statType, page) {
    switch (statType) {
      case 0:
        this.getSalsePayType(page);
        break;
      case 1:
        this.getSalesMenu();
        break;
      case 2:
        this.getPayHistory(page);
        break;
      case 3:
        this.getCashPrint(page);
        break;
      case 4: break;
      case 5:
        this.getEnd(page);
        break;
      case 6:
        this.getSettlement(page);
        break;
      default: break;
    }
  }

  //필터 선택시 
  selectedFilter(category, filter) {
    if (category == 3) {//메뉴 카테고리 일때 
      this.statFilter = this.category3[filter]['categoryNo'];//메뉴 카테고리는 메뉴 넘버를 반환 해야해 
      this.selectedCategoryIdx = filter;
      this.showDrop = false;
    } else {
      this.statFilter = filter;
      this.selectedCategoryIdx = filter;
      this.showDrop = false;
    }
    this.selectedStatType(this.statType, 1);
  }

  //날짜 변경시 
  showDatePickerChange(event) {
    this.showDatePicker = event;
  }

  //날짜 - 기간 선택 
  selectPeriod(period) {
    this.selectedPeriod = period;
    if (period == 1) {
      this.startDate = this.today; this.endDate = this.today;
    }
    else if (period == 2) {
      this.startDate = new Date(this.endDate.getTime() - (7 * 24 * 60 * 60 * 1000));
    }
    else if (period == 3) {
      this.startDate = new Date(this.endDate.getTime() - (31 * 24 * 60 * 60 * 1000));
    }
    else if (period == 4) {
      this.startDate = new Date(this.endDate.getTime() - (93 * 24 * 60 * 60 * 1000));
    }
    this.pageNo = 1;
    this.selectedStatType(this.statType, this.pageNo);
  }

  clickDatePicker(type) {
    let dateModal;

    if (type == 1)
      dateModal = this.modal.create('DatePickerModalPage', { date: this.startDate }, { enableBackdropDismiss: false, cssClass: 'date-modal' });
    else
      dateModal = this.modal.create('DatePickerModalPage', { date: this.endDate }, { enableBackdropDismiss: false, cssClass: 'date-modal' });

    dateModal.onDidDismiss(data => {
      if (type == 1) {
        if (this.today < data.date) {
          this.startDate = this.today;
          this.toast('오늘 이후의 날짜는 선택 하실 수 없습니다.');
        } else {
          this.startDate = data.date;
          if (this.startDate > this.endDate)
            this.endDate = this.startDate;
        }
      }
      else {
        if (this.today < data.date) {
          this.endDate = this.today;
          this.toast('오늘 이후의 날짜는 선택 하실 수 없습니다.');
        } else {
          this.endDate = data.date
          if (this.startDate > this.endDate)
            this.startDate = this.endDate;
        }
      }
      this.selectedStatType(this.statType, this.pageNo);
    });
    dateModal.present();

  }

  //infinite scroll시 불러오는 
  OnPageChanged() {
    this.pageNo++;
    this.selectedStatType(this.statType, this.pageNo);

  }
  //page변경(매출 조회, 결제 내역, 마감 내역 .. )될때 
  statPageChange(page) {
    switch (page) {
      case 0:
        this.selectedStatBt(0);
        break;
      case 1:
        this.selectedStatBt(2);
        break;
      case 2:
        this.selectedStatBt(5);
        break;
      case 3:
        this.selectedStatBt(6);
        break;
      default: break;
    }
  }
  //stat data (list) 들고 옴 
  //stat type ==0 
  getSalsePayType(pageNo = 1) {
    this.statSumData = [0, 0, 0, 0];
    let datePipe = new DatePipe('en-US');
    var chooseStartDate = datePipe.transform(this.startDate, 'yyyy-MM-dd');
    var chooseEndDate = datePipe.transform(this.endDate, 'yyyy-MM-dd');
    this.log.salesPaytype.get(chooseStartDate, chooseEndDate, this.statFilter, pageNo)
      .subscribe(
        data => {
          if (this.statFilter == 0)
            this.statTotal = 3;
          else
            this.statTotal = data.json().total;

          if (pageNo == 1) {
            this.statData = [];
            if (data.json().list.length == 0)
              this.toast('결제 수단 별 매출 통계가 없습니다. 일자를 확인해 주세요 ');
            else
              this.toast('결제 수단 별 매출 통계를 가지고 왔습니다.');
          }

          if (this.statTotal - this.statData.length > 0) {
            this.statData = this.statData;
            var i = 0;
            for (i = 0; i < data.json().list.length; i++) {
              this.statData.push({
                payAmt: data.json().list[i].payAmt,
                payCNT: data.json().list[i].payCNT,
                payTax: data.json().list[i].payTax,
                payTotal: data.json().list[i].payTotal,
                payType: data.json().list[i].payType,
                cardName: data.json().list[i].cardName
              });
            }
            for (i = 0; this.statData.length > i; i++) {
              this.statSumData[0] += this.statData[i]['payCNT'];
              this.statSumData[1] += this.statData[i]['payAmt'];
              this.statSumData[2] += this.statData[i]['payTax'];
              this.statSumData[3] += this.statData[i]['payTotal'];
              if (this.statFilter == 0)
                this.statData[i]['payTypeName'] = this.payType[this.statData[i]['payType']];
            }
            if (this.statFilter == 0)
              this.statTotal = this.statData.length;
            // this.r detect 이거 추가하기 
          }
        },
        err => {
          this.statData = [];
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('오류입니다. 다시시도해 주세요.');
        }
      );
  }
  //stat type ==1 
  getSalesMenu() {
    this.statSumData = [0, 0, 0, 0];
    let datePipe = new DatePipe('en-US');
    var chooseStartDate = datePipe.transform(this.startDate, 'yyyy-MM-dd');
    var chooseEndDate = datePipe.transform(this.endDate, 'yyyy-MM-dd');
    this.log.salesMenu.get(chooseStartDate, chooseEndDate, this.statFilter)
      .subscribe(
        data => {
          this.statTotal = 0;

          this.statData = [];
          if (data.json().list.length == 0)
            this.toast('상품 별 매출 통계가 없습니다. 일자를 확인해 주세요 ');
          else {
            this.toast('상품 별 매출 통계를 가지고 왔습니다.');
            if (data.json().list.length > 0) {
              let temp = _.cloneDeep(this.statData);
              var i = 0;
              for (i = 0; i < data.json().list.length; i++) {
                temp.push({
                  menuCNT: data.json().list[i].menuCNT,
                  menuName: data.json().list[i].menuName,
                  payAmt: data.json().list[i].payAmt,
                  payTax: data.json().list[i].payTax,
                  payTotal: data.json().list[i].payTotal,
                });
              }
              this.statData = _.cloneDeep(temp);
              for (i = 0; this.statData.length > i; i++) {
                this.statSumData[0] += this.statData[i]['menuCNT'];
                this.statSumData[1] += this.statData[i]['payAmt'];
                this.statSumData[2] += this.statData[i]['payTax'];
                this.statSumData[3] += this.statData[i]['payTotal'];
              }
            }


          }
        },
        err => {
          this.statData = [];
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('오류입니다. 다시시도해 주세요.');
        }
      );

  }
  //stat type ==2 
  getPayHistory(pageNo = 1) {
    let datePipe = new DatePipe('en-US');
    var chooseStartDate = datePipe.transform(this.startDate, 'yyyy-MM-dd');
    var chooseEndDate = datePipe.transform(this.endDate, 'yyyy-MM-dd');

    this.pay.getHistory(chooseStartDate, chooseEndDate, this.statFilter, pageNo)
      .subscribe(
        data => {
          this.statTotal = data.json().total;
          if (pageNo == 1) {
            this.statData = [];
            if (data.json().list.length == 0)
              this.toast('결제 내역이 없습니다. 일자를 확인해 주세요 ');
            else
              this.toast('결제 내역을 가지고 왔습니다.');
          }
          if (this.statTotal - this.statData.length > 0) {
            let temp = _.cloneDeep(this.statData);
            for (var i = 0; i < data.json().list.length; i++) {
              temp.push({
                cardName: data.json().list[i].cardName,
                floorNo: data.json().list[i].floorNo,
                orderAmt: data.json().list[i].orderAmt,
                orderDttm: data.json().list[i].orderDttm,
                orderName: data.json().list[i].orderName,
                orderNo: data.json().list[i].orderNo,
                payNo: data.json().list[i].payNo,
                payTotal: data.json().list[i].payTotal,
                payType: data.json().list[i].payType,
                tables: data.json().list[i].tables,
                watingNo: data.json().list[i].watingNo,
                tableName: data.json().list[i].tableName,
                cardID: data.json().list[i].cardID,
                approvalDttm: data.json().list[i].approvalDttm,
                approvalKey: data.json().list[i].approvalKey
              });
            }
            this.statData = _.cloneDeep(temp);
          }


        },
        err => {
          this.statData = [];
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('오류입니다. 다시시도해 주세요.');
        }
      );
  }
  //stat type ==3
  getCashPrint(pageNo = 1) {

    let datePipe = new DatePipe('en-US');
    var chooseStartDate = datePipe.transform(this.startDate, 'yyyy-MM-dd');
    var chooseEndDate = datePipe.transform(this.endDate, 'yyyy-MM-dd');

    this.log.cashPrint.get(chooseStartDate, chooseEndDate, pageNo)
      .subscribe(
        data => {
          this.statTotal = data.json().total;
          var i = 0;
          if (pageNo == 1) {
            this.statData = [];
            if (data.json().list.length == 0)
              this.toast('현금 영수증 내역이 없습니다. 일자를 확인해 주세요 ');
            else
              this.toast('현금 영수증 내역을 가지고 왔습니다.');
          }
          if (this.statTotal - this.statData.length > 0) {
            let temp = _.cloneDeep(this.statData);
            for (i = 0; i < data.json().list.length; i++) {
              this.statData.push({
                cashAmt: data.json().list[i].cashAmt,
                cashPrint: data.json().list[i].cashPrint,
                payDttm: data.json().list[i].payDttm,
                payNo: data.json().list[i].payNo
              });
            }
            this.statData = _.cloneDeep(temp);

          }
          for (i = 0; i < data.json().list.length; i++) {
            this.statData.push({
              cashAmt: data.json().list[i].cashAmt,
              cashPrint: data.json().list[i].cashPrint,
              payDttm: data.json().list[i].payDttm,
              payNo: data.json().list[i].payNo
            });
          }

        },
        err => {
          this.statData = [];
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('오류입니다. 다시시도해 주세요.');
        }
      );
  }
  //stat type ==5 
  getEnd(pageNo = 1) {
    let datePipe = new DatePipe('en-US');
    var chooseStartDate = datePipe.transform(this.startDate, 'yyyy-MM-dd');
    var chooseEndDate = datePipe.transform(this.endDate, 'yyyy-MM-dd');
    this.sales.end.get(chooseStartDate, chooseEndDate, 0, pageNo)
      .subscribe(
        data => {
          this.statTotal = data.json().total;

          if (pageNo == 1) {
            this.statData = [];
            if (data.json().list.length == 0)
              this.toast('마감관리 내역이 없습니다. 일자를 확인해 주세요 ');
            else
              this.toast('마감관리 내역을 가지고 왔습니다.');
          }
          if (this.statTotal - this.statData.length > 0) {
            let temp = _.cloneDeep(this.statData);
            for (var i = 0; i < data.json().list.length; i++) {
              temp.push({
                balanceCash: data.json().list[i].balanceCash,
                closeDttm: data.json().list[i].closeDttm,
                issueMileage: data.json().list[i].issueMileage,
                openCash: data.json().list[i].openCash,
                salesDttm: data.json().list[i].salesDttm,
                totalCard: data.json().list[i].totalCard,
                totalCash: data.json().list[i].totalCash,
                totalMileage: data.json().list[i].totalMileage,
                totalPoint: data.json().list[i].totalPoint
              });
            }
            this.statData = _.cloneDeep(temp);
            this.statData.forEach((element) => {
              element['payTotal'] = element.totalCard + element.totalCash;
              element['salesDt'] = datePipe.transform(element.salesDttm, 'yyyy-MM-dd');;
            });

          }
        },
        err => {
          this.statData = [];
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('오류입니다. 다시시도해 주세요.');
        }
      );
  }

  //stat type ==6 
  getSettlement(pageNo = 1) {
    this.ref.detectChanges();
    this.statData=[]
            let temp =[];
              temp.push({
                settleBetween: this.tempSTData.TOTAL.FROM_DT + " ~ " + this.tempSTData.TOTAL.TO_DT,
                pgRate: this.tempSTData.TOTAL.PG_RATE,
                transRate: this.tempSTData.TOTAL.TRANS_RATE,
                count: this.tempSTData.TOTAL.COUNT,
                TotalAmt: this.tempSTData.TOTAL.TOTAL_AMT,
                totalDepositAmt: this.tempSTData.TOTAL.TOTAL_DEPOSIT_AMT,
                totalGeneralFee: this.tempSTData.TOTAL.TOTAL_GENERAL_FEE,
                totalNoIntFee: this.tempSTData.TOTAL.TOTAL_NO_INT_FEE,
                totalGeneralVat: this.tempSTData.TOTAL.TOTAL_GENERAL_VAT,
                totalNoIntVat: this.tempSTData.TOTAL.TOTAL_NO_INT_VAT,
                list: this.tempSTData.LIST
              });
            this.statData = _.cloneDeep(temp);
  }
  //버튼 클릭시 
  //결제 취소 
  goPayDelete() {
    // let modal = this.modalCtrl.create(ConfirmAlertPage, { title: '결제 취소', label_big: '선택된 주문의 모든 결제가 취소 됩니다', label_small: '결제를 취소하고 다시 계산하시겠습니까?', type: 0 }, {
    //   cssClass: 'message-modal'
    // });
    if (this.selectedRowData.payNo == undefined || this.selectedRowData.orderNo == undefined)
      this.toast('주문 혹은 결제를 선택해 주세요');
    else if (this.selectedRowData.payNo == 0)
      this.toast('복합 결제입니다. 해당 주문의 복합 버튼을 클릭해 주세요');
    else {
      let modal = this.modalCtrl.create('ConfirmAlert2Page', { title: '결제 취소', label_big: '결제를 취소하시겠습니까?', label_payType: this.selectedRowData.payType, label_payAmt: this.selectedRowData.payTotal }, {
        cssClass: 'message-modal'
      });
      modal.onDidDismiss(data => {
        console.log(data);
        if (data.res == true) {
          if (this.selectedRowData.payType == '카드') {
            let modal = this.modalCtrl.create('ConfirmAlert2Page', { title: 'IC 카드 삽입', label_big: 'IC 카드를 삽입해 주세요.', label_payType: '카드', label_payAmt: this.selectedRowData.payTotal }, {
              cssClass: 'message-modal'
            });
            modal.onDidDismiss(data => {
              if (data.res == true)
                this.paydelete(true);
              //결제 단말기와 합쳐야할 부분 
              else
                this.toast('IC 삽입이 취소되었습니다. 결제 취소를 다시 시도해 주세요');
            });
            modal.present();
          } else {
            this.paydelete(true);
          }
        }
        else {//결제 취소를 취소 
          this.toast('취소되었습니다.');
        }
      });
      modal.present();
    }
  }

  goPayModify() {
    if (this.selectedRowData.payNo == undefined || this.selectedRowData.orderNo == undefined)
      this.toast('주문 혹은 결제를 선택해 주세요');
    else if (this.selectedRowData.payNo == 0)
      this.toast('복합 결제입니다. 해당 주문의 복합 버튼을 클릭해 주세요');
    else if (this.selectedRowData.payType == '포인트' || this.selectedRowData.payType == '마일리지')
      this.toast('결제 수단 변경은 카드 결제와 현금 결제만 가능합니다.');
    else {
      let modal = this.modalCtrl.create('ConfirmAlert2Page', { title: '결제 수단 변경', label_big: '기존 결제를 취소하시겠습니까?', label_payType: this.selectedRowData.payType, label_payAmt: this.selectedRowData.payTotal }, {
        cssClass: 'message-modal'
      });
      modal.onDidDismiss(data => {
        console.log(data);
        if (data.res == true) { // 결제 취소를 합니다
          if (this.selectedRowData.payType == '카드') {
            let modal = this.modalCtrl.create('ConfirmAlert2Page', { title: 'IC 카드 삽입', label_big: 'IC 카드를 삽입해 주세요.', label_payType: '카드', label_payAmt: this.selectedRowData.payTotal }, {
              cssClass: 'message-modal'
            });
            modal.onDidDismiss(data => {
              if (data.res == true)
                this.paydelete(false);
              //결제 단말기와 합쳐야할 부분 
              else
                this.toast('IC 삽입이 취소되었습니다. 결제 수단 변경을 다시 시도해 주세요');
            });
            modal.present();
          } else if (this.selectedRowData.payType == '현금') {
            this.paydelete(false);
          } else {
            this.toast('현금과 카드 결제일때만 결제 수단 변경이 가능합니다. 결제 수단을 확인해 주세요.');
          }
        }
        else {//결제 수단 변경을 취소 
          this.toast('취소되었습니다.');
        }
      });
      modal.present();
    }
  }

  paydelete(val) {
    //val = true 결제 취소, val = false 결제 수단 변경 
    this.pay.delete(this.selectedRowData.payNo, this.selectedRowData.orderNo)
      .subscribe(
        data => {
          if (val) {
            this.toast('결제 취소에 성공했습니다. 결제 화면으로 넘어갑니다.');
            console.log(this.selectedRowData.orderNo);
            console.log(data.json()[0].remain);
            this.events.publish('nav:goPay', this.selectedRowData.orderNo, data.json()[0].remain);
          }
          else {
            this.toast('결제 수단 변경을 위해 기존 결제 취소에 성공했습니다.');
            this.rePay();
          }
        },
        err => {
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('오류입니다. 다시시도해 주세요.');
        });
  }

  rePay() {
    let modal;
    if (this.selectedRowData.payType == '카드') {
      modal = this.modalCtrl.create('MessageAlertPage', { title: '결제 취소 후 현금 계산', message: '현금으로  ' + this.selectedRowData.payTotal + '원을 받아주세요', color: 'gray', closeAll: false }, { cssClass: "message-modal" });
      modal.onDidDismiss(() => {
        this.payCash();
      });
    }
    if (this.selectedRowData.payType == '현금') {
      modal = this.modalCtrl.create('ConfirmAlert2Page', { title: 'IC 카드 삽입', label_big: 'IC 카드를 삽입해 주세요.', label_payType: '카드', label_payAmt: this.selectedRowData.payTotal }, {
        cssClass: 'message-modal'
      });
      modal.onDidDismiss(data => {
        if (data.res == true) {
          this.payCard();
        } else {
          this.toast('카드 결제 해주세요.');
          this.rePay();
        }
      });
    }
    modal.present();
  }

  //결제 하기 
  payCash() {
    //현금 쓰기
    // var payTax = Math.floor(this.dividePayTotal / this.taxRatio * 0.1);
    this.pay.postCash(
      this.selectedRowData.orderNo, //주문 번호 
      this.selectedRowData.payTotal,//총 주문 금액 
      this.selectedRowData.payTotal,//cashAmt
      0,//mileageAmt
      0,//pointAmt
      null,//cashPrint
      null,//barcode
      null,//userNo
      null//curPoint
    )
      .subscribe(
        data => {
          this.toast('현금 결제 성공!');
          this.selectedStatType(this.statType, this.pageNo);
          this.selectedRowData = {};
        },
        err => {
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('현금 결제 실패! 다시 시도해 주세요');
        }
      );
  }

  payCard() {
    //카드 쓰기 
    this.pay.postCard(
      this.selectedRowData.orderNo, //주문 번호 
      this.selectedRowData.payTotal,//총 주문 금액 
      this.selectedRowData.payTotal,//cashAmt
      null,//mileageAmt
      null,//pointAmt
      this.cardPayInfo.cardID,
      this.cardPayInfo.cardType,
      this.cardPayInfo.cardName,
      this.cardPayInfo.cardCompany,
      1,
      this.cardPayInfo.cardYYMM,
      this.cardPayInfo.approvalAmt,
      this.cardPayInfo.approvalNo,
      this.cardPayInfo.terminalStoreID,
      this.cardPayInfo.terminalID,
      this.cardPayInfo.tranID,
      null,//barcode
      null,//userNo
      null//curPoint
    )
      .subscribe(
        data => {
          this.toast('카드 결제 성공!');
          this.selectedStatType(this.statType, this.pageNo);
          this.selectedRowData = {};
        },
        err => {
          if (err == "network")
            this.toast('인터넷 연결을 확인해 주세요.');
          else
            this.toast('카드 결제 실패! 다시 시도해 주세요.');
        }
      );
  }

  //현금 영수증  
  cashPrint() {
    console.log(this.selectedRowData);
    if (this.selectedRowData.payNo == undefined || this.selectedRowData.orderNo == undefined)
      this.toast('주문 혹은 결제를 선택해 주세요');
    else if (this.selectedRowData.payType != '현금')
      this.toast('현금으로 주문한 건만 현금영수증 발행이 가능합니다.');
    else if (this.selectedRowData.payTotal < 0)
      this.toast('취소 건은 현금영수증을 발급 할 수 없습니다.');
    else {
      let modal = this.modalCtrl.create('CashPrintPage', { data: this.selectedRowData, modified: true }, { cssClass: 'mileage-modal', enableBackdropDismiss: false });
      modal.onDidDismiss(data => {
        if (!data)
          this.toast('현금 영수증이 취소되었습니다.');
      });
      modal.present();
    }
  }

  //기능 준비중 
  preparing() {
    let modal = this.modalCtrl.create('MessageAlertPage', { title: '준비중', message: '준비중인 기능입니다.', color: 'red' }, { cssClass: "message-modal" });
    modal.present();
  }

  //영수증 출력
  receiptPrint() {
    if (this.selectedRowData.payNo == undefined || this.selectedRowData.orderNo == undefined)
      this.toast('주문 혹은 결제를 선택해 주세요');
    else {
      let modal = this.modalCtrl.create('MessageAlertPage', { title: '영수증 출력 완료', message: '감사합니다.', color: 'gray' }, { cssClass: "message-modal" });
      modal.present();
    }

  }

  //마감 취소 
  endDelete() {
    // this.preparing();
    let modal = this.modalCtrl.create('ConfirmAlertPage', { title: '마감 취소', label_big: '최근에 마감한 마감을 취소하시겠습니까?', label_small: '취소하시면 계속해서 영업 하실 수 있습니다.', type: 0 }, {
      cssClass: 'message-modal'
    });
    modal.onDidDismiss(data => {
      console.log(data);
      if (data.res == true) {
        this.sales.end.delete()
          .subscribe(
            data => {
              if (data.status == 201) {
                this.selectedStatType(this.statType, this.pageNo);
                this.toast('마감 취소를 했습니다. ');
              }
              // console.log(data.json());
            },
            err => {
              if (err == "network")
                this.toast('인터넷 연결을 확인해 주세요.');
              else if (err.json().message == "마감되어있지 않습니다.")
                this.toast('아직 영업 중입니다. 마감 된 상태에서 마감 취소가 가능합니다.');
              else
                this.toast('오류입니다. 다시시도해 주세요.');
            }
          );
      }
      else {
        this.toast('취소되었습니다.. ');
      }
    });
    modal.present();
  }

  //list의 눈 모양 클릭시 
  goEndDetail(selectedData) {
    let datePipe = new DatePipe('en-US');
    let date = datePipe.transform(selectedData.salesDttm, 'yyyy-MM-dd');
    let modal = this.modalCtrl.create('StatMenuDailyPage', { startDate: date, endDate: date }, {
      cssClass: 'mileage-modal'
    });
    modal.present();

  }

  goOrderDetail(selectedData) {
    // selectedData.orderNo;
    let modal = this.modalCtrl.create('StatMenuOrderPage', { orderNo: selectedData.orderNo }, {
      cssClass: 'mileage-modal'
    });
    modal.present();

  }
  //복합 클릭시
  goComplex(selectedData) {
    let modal = this.modalCtrl.create('StatPayComplexPage', { orderNo: selectedData.orderNo }, {
      cssClass: 'mileage-modal'
    });
    modal.present();
  }

  goSettleDetail(selectedData){
    console.log(this.statData)
    let modal = this.modalCtrl.create('StatPaySettlePage', { settleDetail: selectedData.list }, {
      cssClass: 'full-modal'
    });
    modal.present();
  }


  toast(text) {
    if (this.toastInstance) {
      return;
    }
    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
    });
    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });
    this.toastInstance.present();
  }
}
