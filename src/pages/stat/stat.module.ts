import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StatPage } from './stat';
import { JustPickDateComponentModule } from '../../components/just-pick-date/just-pick-date.module';
import { DatepickerModule } from '../../components/datepicker/datepicker.module';
import { TableComponentModule } from '../../components/table/table.module';



@NgModule({
  declarations: [
    StatPage,
  ],
  imports: [
    IonicPageModule.forChild(StatPage),
    JustPickDateComponentModule,
    DatepickerModule,
    TableComponentModule
  ],
  exports: [
    StatPage
  ]
})
export class StatPageModule {}
