import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Events, MenuController } from 'ionic-angular';
import { StatPage } from '../stat/stat';
import { LoginPage } from '../login/login';
import { MembershipPage } from '../membership/membership';
import { StoreSettingPage } from '../store-setting/store-setting';
import { OrderPage } from '../order/order';
import { OrderTablePage } from '../order-table/order-table';
import { BeginPage } from '../begin/begin';
import { EndPage } from '../end/end';
import { TranslateService } from '@ngx-translate/core';
import { OrderMenuListPage } from '../order-menu-list/order-menu-list';

@IonicPage()
@Component({
  selector: 'page-container',
  templateUrl: 'container.html',
})
export class ContainerPage {
  @ViewChild('myNav') nav: NavController;
  rootPage: any = 'LoginPage';

  //header options
  title: string = "헤더";
  showMenuBtn: boolean = false;
  showBackBtn: boolean = false;
  showRightBtn: boolean = false;
  showBackBtnOrder: boolean = false;

  backgroundHeight: string = '15%';

  showBackDrop: boolean = false;

  minHeight: number = 0;

  lang;

  constructor(public navCtrl: NavController, public events: Events, private menuCtrl: MenuController, private tran: TranslateService) {
    events.subscribe('layout:header', (title,showRightBtn,showBackBtn,showMenuBtn, showBackBtnOrder = false) => {
      this.title = title;
      this.showMenuBtn = showMenuBtn;
      this.showBackBtn = showBackBtn;
      this.showRightBtn = showRightBtn;
      this.showBackBtnOrder = showBackBtnOrder;
    });
    events.subscribe('layout:header-background', (height) => {
      this.backgroundHeight = height;
    });
    events.subscribe('layout:backdrop', (tf) => {
      this.showBackDrop = tf;
    });
    events.subscribe('nav:go', (index) => {
      switch (index) {
        case 0:
          if (this.nav && this.nav.getActive().component != OrderPage) {
            this.nav.setRoot('OrderPage').then(()=>{
              this.events.publish('nav:goToTable');
            });
          }
          else if (this.nav && this.nav.getActive().component == OrderPage && this.nav.getActiveChildNav().getActive().component != OrderTablePage) {
            this.nav.getActiveChildNav().setRoot('OrderTablePage');
            // this.events.publish('order:get', {}, -1);
          }
          this.events.publish('nav:index', 0);
          break;
        case 1:
          if (this.nav && this.nav.getActive().component != StatPage) {
            this.nav.setRoot('StatPage');
          }
          this.events.publish('nav:index', 1);
          break;
        case 2:
          if (this.nav && this.nav.getActive().component != MembershipPage) {
            this.nav.setRoot('MembershipPage');
          }
          this.events.publish('nav:index', 2);
          break;
        case 3:
          if (this.nav && this.nav.getActive().component != StoreSettingPage) {
            this.nav.setRoot('StoreSettingPage');
          }
          this.events.publish('nav:index', 3);
          break;
        case 4:
          if (this.nav && this.nav.getActive().component != BeginPage) {
            this.nav.setRoot('BeginPage');
          }
          this.events.publish('nav:index', 4);
          break;
        case 5:
          if (this.nav && this.nav.getActive().component != LoginPage) {
            this.nav.setRoot('LoginPage');
          }
          this.events.publish('nav:index', 5);
          break;
        case 6:
          if (this.nav && this.nav.getActive().component != EndPage) {
            this.nav.setRoot('EndPage');
          }
          this.events.publish('nav:index', 6);
          break;
      }
    });
  }
  
  setLang(lang) {
    this.lang = lang;
    this.tran.use(lang).subscribe(_=>console.log(lang));
    this.events.publish('lang:changed', lang);
  }

  ionViewDidLoad() {    
    this.events.publish('layout:header', '헤더', true, true, true);
    setTimeout(() => {
      this.minHeight = document.getElementById('content').offsetHeight;
    }, 0);
  }

  // ionViewDidLeave() {
  //   this.events.unsubscribe('layout:header');
  //   this.events.unsubscribe('layout:header-background');
  //   this.events.unsubscribe('layout:backdrop');
  //   this.events.unsubscribe('nav:go');
  // }

  open() {
    this.menuCtrl.open();
  }

  back() {
    this.nav.pop();
  }
}
