import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, ToastController, Toast } from 'ionic-angular';
import { Sales } from '../../services/sales';

@IonicPage()
@Component({
  selector: 'page-stat-menu-order',
  templateUrl: 'stat-menu-order.html',
})
export class StatMenuOrderPage {
  title: string = "상품 내역";
  orderNo: any;
  data: any[] = [];
  total: number = 0;
  pageNo: number = 1;

  private toastInstance: Toast;
  constructor(public navCtrl: NavController, public navParams: NavParams, private sales: Sales, private viewCtrl: ViewController, public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad StatMenuOrderPage');
    this.orderNo = this.navParams.get('orderNo');
    this.getSalesMenu();
  }

  getSalesMenu(pageNo = 1) {
    this.sales.order.getDetail(this.orderNo, pageNo)
      .subscribe(
      data => {
        if (pageNo == 1) {
          this.data = [];
        }
        if ( data.json().list.length != 0)
          data.json().list.forEach(element => {
            this.data.push({
              dcAmt: element.dcAmt,
              dcName: element.dcName,
              itemAmt: element.itemAmt,
              itemCNT: element.itemCNT,
              menuName: element.menuName,
              menuNo: element.menuNo,
              orderMemo: element.orderMemo,
              orderNo: element.orderNo
            })
          });
      },
      err => {
        this.data = [];
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else
          this.toast('오류입니다. 다시시도해 주세요.');
      }
      );

  }

  get orderSum(): number {
    var sum: number = 0;
    if (this.data)
      this.data.forEach(element => {
        sum += (element.itemCNT * element.itemAmt) - (element.itemCNT * element.dcAmt);
      });
    return sum;
  }

  doInfinite() {
    this.pageNo++;
    this.getSalesMenu(this.pageNo);
  }

  toast(text) {
    if (this.toastInstance) {
      return;
    }
    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
    });
    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });
    this.toastInstance.present();
  }

  close() {
    this.viewCtrl.dismiss();
  }


}
