import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events } from 'ionic-angular';
import { Setting } from '../../services/setting';

@IonicPage()
@Component({
  selector: 'page-deleted-menu',
  templateUrl: 'deleted-menu.html',
})
export class DeletedMenuPage {
  title: string = "최근 삭제메뉴";
  menus: Array<any> = [];
  selectedMenuIdx: number = -1;

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private setting: Setting, private events: Events) {
  }

  ionViewDidLoad() {
    this.getMenu();
  }

  getMenu() {
    this.setting.menu.bin.get()
    .subscribe(
      data => {
        this.menus = data.json();
        this.selectedMenuIdx = -1;
        console.log(this.menus);
      }
    );
  }

  restoreMenu() {
    if(this.selectedMenuIdx == -1)
      return ;
    this.setting.menu.bin.put(this.menus[this.selectedMenuIdx].menuNo)
    .subscribe(
      data => {
        console.log('메뉴복구');
        this.getMenu();
        this.events.publish('menu-list:refresh');
      }
    );
  }

  deleteMenu() {
    if(this.selectedMenuIdx == -1)
      return ;
    this.setting.menu.bin.delete(this.menus[this.selectedMenuIdx].menuNo)
    .subscribe(
      data => {
        console.log('메뉴영구삭제');
        this.getMenu();
        this.events.publish('menu-list:refresh');
      }
    );
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
