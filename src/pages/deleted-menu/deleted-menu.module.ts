import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeletedMenuPage } from './deleted-menu';
import { AlertHeaderComponentModule } from '../../components/alert-header/alert-header.module';
import { SquareModule } from '../../directives/square/sqare.module';

@NgModule({
  declarations: [
    DeletedMenuPage,
  ],
  imports: [
    IonicPageModule.forChild(DeletedMenuPage),
    AlertHeaderComponentModule,
    SquareModule
  ],
  exports: [
    DeletedMenuPage
  ]
})
export class DeletedMenuPageModule {}
