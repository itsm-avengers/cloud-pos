import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Events, ToastController, Toast } from 'ionic-angular';
import { Sales } from '../../services/sales';
import { DatePipe } from '@angular/common';

@IonicPage()
@Component({
  selector: 'page-sales-amt-modify',
  templateUrl: 'sales-amt-modify.html',
})
export class SalesAmtModifyPage {
  title: string = "영업 준비금 수정";
  salesDttm: any;
  salesDt: any;
  balanceCash: number = 0;
  amtPlus: number = 0;
  amtMinus: number = 0;
  totalAmt: number = 0;

  private toastInstance: Toast;


  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private sales: Sales, private events: Events, public toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    this.getOpenTm();
  }

  //가장 최근의 개시 일자 가지고 오기 
  getOpenTm() {
    this.sales.end.getNo()
      .subscribe(
      data => {
        this.salesDttm = data['_body'];
        let datePipe = new DatePipe('en-US');
        this.salesDt = datePipe.transform(this.salesDttm, 'yyyy-MM-dd');
        this.getEndData(this.salesDt);
        console.log(data);
      });
  }
  //가장 최근의 개시일자에 해당하는 마감 data 들고오기 
  getEndData(salesDttm) {
    this.sales.end.get(salesDttm, salesDttm, 1)
      .subscribe(
      data => {
        this.totalAmt = this.balanceCash = data.json().list[0].balanceCash;
        console.log(data.json());
      });
  }

  calculate(event, val) {
    if(val)
    this.amtPlus = event.keyCode ? event.keyCode : event.which;
    else 
    this.amtMinus = event.keyCode ? event.keyCode : event.which;
    
    // var inputKeyCode = event.keyCode ? event.keyCode : event.which;
    this.totalAmt = this.balanceCash + this.amtPlus - this.amtMinus;
  }

  modifySalesAmt() {
    this.sales.end.put(this.salesDttm, this.totalAmt)
      .subscribe(
      data => {
        this.toast('시제금 수정에 성공했습니다.');
        this.close();
      },
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else
          this.toast('오류입니다. 다시시도해 주세요.');
      });
  }



  close() {
    this.viewCtrl.dismiss();
  }

  toast(text) {
    if (this.toastInstance) {
      return;
    }
    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
      cssClass: 'custom-toast',
    });
    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });
    this.toastInstance.present();
  }
}
