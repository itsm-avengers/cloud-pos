import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ToastController, Events, Content, ModalController, Toast, IonicPage } from 'ionic-angular';
import { ConnectorService } from "../../modules/Numpad";
import * as moment from 'moment'
import 'moment/locale/ko'
import { FormGroup, FormControl } from "@angular/forms";
import { Sales } from '../../services/sales';
import { JustPickDateComponent } from '../../components/just-pick-date/just-pick-date';
import { LoginSession } from '../../services/login.session';
import { DatePipe } from "@angular/common";

@IonicPage()
@Component({
  selector: 'page-begin',
  templateUrl: 'begin.html'
})
export class BeginPage {
  @ViewChild('jpd') jpd: JustPickDateComponent;

  cash: Array<any> = [
    { name: '10만원권', value: 100000, cnt: 0 },
    { name: '5만원권', value: 50000, cnt: 0 },
    { name: '1만원권', value: 10000, cnt: 0 },
    { name: '5천원권', value: 5000, cnt: 0 },
    { name: '1천원권', value: 1000, cnt: 0 },
    { name: '500원', value: 500, cnt: 0 },
    { name: '100원', value: 100, cnt: 0 },
    { name: '50원', value: 50, cnt: 0 },
    { name: '10원', value: 10, cnt: 0 }
  ];

  today = new Date();
  endData: any = [{ salesDt: '' }];

  private toastInstance: Toast;

  constructor(public navCtrl: NavController, public navParams: NavParams, public numpad: ConnectorService, public sales: Sales, public toastCtrl: ToastController, private events: Events, private modal: ModalController, private loginSession: LoginSession, private modalCtrl: ModalController) { }

  ionViewDidLoad() {
    this.events.publish('layout:header', '영 업 - 영 업 준 비', true, false, true);
    // this.getEndData();
  }

  sum() {
    var sum: number = 0;
    for (let coin of this.cash) {
      sum += coin.value * coin.cnt;
    }
    return sum;
  }
  goBegin() {
    let modal = this.modalCtrl.create('BeginConfirmAlertPage', { title: '영업을 시작하시겠습니까?', salesDt: this.today, openCash: this.sum() }, { cssClass: "message-modal" });
    modal.onDidDismiss((data) => {
      if (data.res == true)
        this.begin();
    });
    modal.present();
  }

  // getEndData(){
  //마감 내역 가지고 오기 지금 디자인에서 없어짐 
  //   var startDate = this.today.getFullYear() + '-' + + (this.today.getMonth() < 10 ? '0' : '') + (this.today.getMonth() + 1) + '-' + '01';
  //   var endDate = this.today.getFullYear() + '-' + + (this.today.getMonth() < 10 ? '0' : '') + (this.today.getMonth() + 1) + '-' + '30';

  //   this.sales.end.get(startDate, endDate, 0, 1,30)
  //   .subscribe(
  //   data => {
  //     this.endData.splice(0,this.endData.length); 
  //     let datePipe = new DatePipe('en-US');
  //     for(var i =0; data.json().list.length>i; i++){
  //       let transDate = datePipe.transform(data.json().list[i].salesDttm, 'yyyy-MM-dd');
  //       this.endData.push({salesDt: transDate});
  //     }
  //     console.log(this.endData);
  //   },
  //   err => {
  //     if (err == "network")
  //       this.toast('인터넷 연결을 확인해 주세요.');
  //     else
  //       this.toast('오류입니다. 다시시도해 주세요.');
  //   }
  //   );
  // }

  begin() {
    let type = this.loginSession.getInfo().posType;
    let datePipe = new DatePipe('en-US');
    let chooseDate = datePipe.transform(this.today, 'yyyy-MM-dd');

    this.sales.start.post(this.sum(), chooseDate)
      .subscribe(
      data => {
        this.toast('영업개시!');
        if (type == "1" || type == 1) {
          this.navCtrl.push('OrderPage', {}, { animate: false }); //선불
        }
        // else
        //   this.navCtrl.push(OrderTablePage); //후불
        this.navCtrl.push('OrderPage'), {}, { animate: false }; //선불임 아직 후불 기능 안돼서 !!!!

        console.log(data);
      }
      ,
      err => {
        if (err == "network")
          this.toast('인터넷 연결을 확인해 주세요.');
        else if (err.json().message == "duplication")
          this.toast('날짜 중복되어 영업 개시가 되지 않았습니다. 다른날을 선택해 주세요.');
        else if (err.json().message == "past")
          this.toast('과거 날짜로는 영업개시를 할 수 없습니다.');
        else
          this.toast('오류입니다. 다시시도해 주세요.');
      }
      );
  }

  dateChanged(event) {
    this.today = event;
  }

  clickDatePicker() {
    let dateModal = this.modal.create('DatePickerModalPage', { date: this.today, isBegin: true }, { enableBackdropDismiss: false, cssClass: 'date-modal' });
    dateModal.onDidDismiss(data => {
      if (data)
        this.today = data.date;
    });
    dateModal.present();
  }

  toast(text) {

    if (this.toastInstance) {
      return;
    }

    this.toastInstance = this.toastCtrl.create({
      message: text,
      duration: 3000,
      dismissOnPageChange: true,
    });

    this.toastInstance.onDidDismiss(() => {
      this.toastInstance = null;
    });

    this.toastInstance.present();
  }

  pm(val, idx) {
    if (val == '+')
      this.cash[idx].cnt++;
    else if (val == '-' && this.cash[idx].cnt > 0)
      this.cash[idx].cnt--;
  }
}
