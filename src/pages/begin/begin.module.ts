import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BeginPage } from './begin';
import { DatepickerModule } from '../../components/datepicker/datepicker.module';
import { NumpadModule } from '../../modules/Numpad/numpad.module';
import { SquareModule } from '../../directives/square/sqare.module';
import { JustPickDateComponentModule } from '../../components/just-pick-date/just-pick-date.module';

@NgModule({
  declarations: [
    BeginPage,
  ],
  imports: [
    IonicPageModule.forChild(BeginPage),
    NumpadModule,
    JustPickDateComponentModule,
    DatepickerModule,
  ],
  exports: [
    BeginPage
  ]
})
export class BeginPageModule {}
