import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoginPage } from './login';
import { CheckboxModule } from '../../components/check-box/check-box.module';

@NgModule({
  declarations: [
    LoginPage,
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
    CheckboxModule
  ],
  exports: [
    LoginPage
  ]
})
export class LoginPageModule {}
